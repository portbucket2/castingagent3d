﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SellPanelControl : MonoBehaviour
{
    public static SellPanelControl instance;
    public Text modelTitle;
    
    

    public Text OfferPriceBookingFeeText;
    public Text bonusText;
    public Text modelDealPriceContactFeeText;
    public Text profitText;

    public Image modelPP;
    public Toggle[] requirementToggles;
    public Text[] requirementTogglesLabels;


    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void SellPanelTogglesUpdate(Toggle[] sourceToggles , Text[] sourceLabels, AvatarAttributes avatarAttr, CustomerAttributes cusAttr)
    {
        int reqs = 0;
        int fulfilled = 0;
        int bonus = 0;
        if (sourceToggles.Length != requirementToggles.Length)
        {
            return;
        }
        for (int i = 0; i < sourceToggles.Length; i++)
        {
            if (sourceToggles[i].gameObject.activeSelf)
            {
                requirementToggles[i].gameObject.SetActive(true);
                requirementToggles[i].isOn = sourceToggles[i].isOn;
                reqs += 1;
                if (requirementToggles[i].isOn)
                    fulfilled += 1;
            }
            else
            {
                requirementToggles[i].gameObject.SetActive(false);
            }
            requirementTogglesLabels[i].text = sourceLabels[i].text;
        }
        if(avatarAttr != null)
        {
            modelTitle.text = avatarAttr.title;
            modelDealPriceContactFeeText.text = "$ " + avatarAttr.dealValue;

            OfferPriceBookingFeeText.text = "$ " + avatarAttr.estimatedWorth;
            if (reqs == fulfilled && reqs != 0)
            {
                bonus = (int)((float)avatarAttr.estimatedWorth * 0.5f);
            }
            else if( fulfilled == (reqs - 1) && reqs != 0)
            {
                bonus = (int)((float)avatarAttr.estimatedWorth * 0.25f);
            }
            else
            {
                bonus = 0;
            }
            bonusText.text = "$ " + bonus;
            profitText.text = "$ " + (cusAttr.directorOfferPrice + bonus - avatarAttr.dealValue);

            modelPP.sprite = avatarAttr.charPP;
        }
        
}
}
