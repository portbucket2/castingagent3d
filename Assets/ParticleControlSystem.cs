﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleControlSystem : MonoBehaviour
{
    public static ParticleControlSystem instance;
    public ParticleSystem confettiParticle;
    public ParticleSystem healthFitParticle;
    public ParticleSystem OutfitChangeParticle;
    public ParticleSystem makeUpDoneParticle;
    public ParticleSystem SlapParticle;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ParticlePlay(ParticleSystem particle, float delay = 0)
    {
        if(delay == 0)
        {
            particle.Play();
        }
        else
        {
            StartCoroutine(ParticleDelayCoroutine(particle,delay));
        }
    }

    public IEnumerator ParticleDelayCoroutine(ParticleSystem particle, float t)
    {
        yield return new WaitForSeconds(t);
        particle.Play();

    }
}
