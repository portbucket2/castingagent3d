﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvatarShadersControl : MonoBehaviour
{
    public Material bodyMat;
    public Material dressMat;
    public Material headMat;
    public Material hairMat;
    public Material kajolMat;
    public Material lipstickMat;

    float timeVal;
    

    // Start is called before the first frame update
    void Start()
    {
        //LerpToGroomedLook(kajolMat, 5 , 0.5f, 1f);
        //LerpToGroomedLook(dressMat, 2);
        MatDuplication();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public IEnumerator LerpingCoroutineKajolLipstick(Material mat, float t, float localT)
    {
        float localTimeVal = localT;
        yield return new WaitForSeconds(0);
        if(t > 0)
        {
            localTimeVal += Time.deltaTime;
            //timeVal = localTimeVal;
            if (localTimeVal < t)
            {
                mat.SetFloat("Vector1_37C7E26A", localTimeVal / t);
                StartCoroutine(LerpingCoroutineKajolLipstick(mat ,t, localTimeVal));
            }
        }
        else if (t == 0)
        {
            mat.SetFloat("Vector1_37C7E26A", 1);
        }
        
    }
    public IEnumerator LerpingCoroutineKajolLipstickHalf(Material mat, float t, float localT, float min , float max)
    {
        float localTimeVal = localT;
        yield return new WaitForSeconds(0);
        if (t > 0)
        {
            localTimeVal += Time.deltaTime;
            //timeVal = localTimeVal;
            if (localTimeVal < t)
            {
                float val = min + ((localTimeVal / t) * (max - min));
                mat.SetFloat("Vector1_37C7E26A", val);
                StartCoroutine(LerpingCoroutineKajolLipstickHalf(mat, t, localTimeVal, min, max));
            }
        }
        else if (t == 0)
        {
            mat.SetFloat("Vector1_37C7E26A", 1);
        }

    }
    public IEnumerator LerpingCoroutineTextures(Material mat, float t, float localT)
    {
        float localTimeVal = localT;
        yield return new WaitForSeconds(0);
        if (t > 0)
        {
            localTimeVal += Time.deltaTime;
            //timeVal = localTimeVal;
            if (localTimeVal < t)
            {
                mat.SetFloat("Vector1_3C90AD37", localTimeVal / t);
                StartCoroutine(LerpingCoroutineTextures(mat, t, localTimeVal));
            }
        }
        else if (t == 0)
        {
            mat.SetFloat("Vector1_3C90AD37", 1);
        }

    }

    public void LerpToGroomedLook(Material mat , float time, float min = 0, float max = 1)
    {
        if(mat != null)
        {
            if (mat == kajolMat || mat == lipstickMat)
            {
                //StartCoroutine(LerpingCoroutineKajolLipstick(mat, time, 0));
                StartCoroutine(LerpingCoroutineKajolLipstickHalf(mat, time, 0, min,max));
            }
            else
            {
                StartCoroutine(LerpingCoroutineTextures(mat, time, 0));
            }
        }
        
    }
    public void InstantSnapToGroomedLook(Material mat, float val)
    {
        if (mat != null)
        {
            if (mat == kajolMat || mat == lipstickMat)
            {
                mat.SetFloat("Vector1_37C7E26A", val);
            }
            else
            {
                mat.SetFloat("Vector1_3C90AD37", val);
            }
        }

    }

    public void ShadersUpdatingAsCurrentCondition()
    {
        AvatarAttributes avatarAttr = GetComponent<AvatarAttributes>();
        if(avatarAttr.makeUpState == MakeUpState.MakeUpDone)
        {
            InstantSnapToGroomedLook(headMat, 1);
            InstantSnapToGroomedLook(bodyMat, 1);
            InstantSnapToGroomedLook(kajolMat, 1);
            InstantSnapToGroomedLook(lipstickMat, 1);
        }
        else
        {
            InstantSnapToGroomedLook(headMat,     0);
            InstantSnapToGroomedLook(bodyMat,     0);
            InstantSnapToGroomedLook(kajolMat,    0);
            InstantSnapToGroomedLook(lipstickMat, 0);
        }

        if (avatarAttr.outfitState == OutfitState.OutfitUpdated)
        {
            InstantSnapToGroomedLook(dressMat, 1);
        }
        else
        {
            InstantSnapToGroomedLook(dressMat, 0);
        }

        //Debug.Log("Shaders Updated " + this.gameObject.name);

    }

    public void MatDuplication()
    {
        //Material _bodymat = new Material(Shader.Find("Shader Graphs/textureBlendChangeShader"));
        //bodyMat = _bodymat;

        if (GetComponent<AvatarAttributes>(). customerType == CustomerType.Model)
        {
            Material[] matBody = GetComponent<AvatarAttributes>().skinnedMesh[0].GetComponent<SkinnedMeshRenderer>().materials;
            Material[] matDress = GetComponent<AvatarAttributes>().skinnedMesh[1].GetComponent<SkinnedMeshRenderer>().materials;
            Material[] matHead = GetComponent<AvatarAttributes>().skinnedMesh[2].GetComponent<SkinnedMeshRenderer>().materials;

            bodyMat = matBody[0];
            dressMat = matDress[0];
            headMat = matHead[0];
            lipstickMat = matHead[1];
            kajolMat = matHead[2];
        }
        //Debug.Log("MatDuplicated " + this.gameObject.name);

    }
}
