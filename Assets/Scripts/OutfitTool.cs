﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutfitTool : GroomingTool
{
    public static OutfitTool instance;
    public GameObject outfitHolder;
    public Vector3 holderRootPos;
    public GameObject[] dressMeshes;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        DisappearTool();
        holderRootPos = outfitHolder.transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        TravellingActivityFun();
    }
    public void OutfitPositionUpdate(float frac)
    {
        outfitHolder.transform.localPosition = Vector3.Lerp(holderRootPos, new Vector3(0, 0, 0), frac);
        if(frac >= 1)
        {
            outfitHolder.SetActive(false);
            OutFitChange();
        }
        else
        {
            outfitHolder.SetActive(true);
        }
    }
    public void OutfitVisualReset()
    {
        outfitHolder.SetActive(true);
        outfitHolder.transform.localPosition = holderRootPos;
    }
    public void OutFitChange()
    {
        avatarOnIt.GetComponent<AvatarShadersControl>().LerpToGroomedLook(avatarOnIt.GetComponent<AvatarShadersControl>().dressMat, 0.5f);
    }
    public void OutfitAvatarOnItAssign(GameObject avatar)
    {
        avatarOnIt = avatar;
    }
    public void RightOutfitEnable(AvatarAttributes avatarAttr)
    {
        int id = avatarAttr.avatarOutfitId;
        for (int i = 0; i < dressMeshes.Length; i++)
        {
            dressMeshes[i].SetActive(false);
        }
        dressMeshes[id].SetActive(true);
        Debug.Log("RightOutfitEnabled");

    }
}
