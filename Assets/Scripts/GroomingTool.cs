﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroomingTool : MonoBehaviour
{
    public Vector3 LeftPos ;
    public Vector3 RightPos;
    public Vector3 centerPos;
    public GameObject toolObj;
    public bool travel;
    public Vector3 destination;
    [Header("Avatar")]
    public GameObject avatarOnIt;
    public Vector3 avatarRootPos;
    public Quaternion avatarRootRot;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void CentreTheTool()
    {
        if (toolObj != null)
        {
            toolObj.transform.localPosition = LeftPos;
            destination = centerPos;
            travel = true;
            AppearTool();
        }
            
    }
    
    //public void LeftMoveTheTool()
    //{
    //    destination = LeftPos;
    //    travel = true;
    //}
    public void RightMoveTheTool()
    {
        destination = RightPos;
        travel = true;
        //ActionOnDisAppear();
    }
    public void DisappearTool()
    {
        if (toolObj != null)
        {
            toolObj.transform.localPosition = LeftPos;
            toolObj.SetActive(false);
        }
    }
    public void AppearTool()
    {
        if(toolObj != null)
        {
            toolObj.SetActive(true);
        }
    }
    public void TravellingActivityFun()
    {
        if (travel)
        {
            toolObj.transform.localPosition = Vector3.MoveTowards(toolObj.transform.localPosition, destination, Time.deltaTime * 20);
            if (toolObj.transform.localPosition == destination)
            {
                travel = false;
                if (toolObj.transform.localPosition != centerPos)
                {

                    DisappearTool();
                }
                else if(toolObj.transform.localPosition == centerPos)
                {
                    ActionOnAppear();
                }
            }
        }
        //if (Input.GetKeyDown(KeyCode.G))
        //{
        //    RightMoveTheTool();
        //}
        //if (Input.GetKeyDown(KeyCode.H))
        //{
        //    CentreTheTool();
        //}
    }
    public virtual void ActionOnAppear()
    {
        //Debug.Log("Appear");
    }
    public virtual void ActionOnDisAppear()
    {
        //Debug.Log("DisAppear");
    }
}
