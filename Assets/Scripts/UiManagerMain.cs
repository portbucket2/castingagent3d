﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiManagerMain : GameManagerMain
{
    public GameObject PanelIdle;
    public GameObject PanelDealing;
    public GameObject PanelRobbing;
    public GameObject PanelDealDone;
    public GameObject PanelDailyReport;
    public Button ButtonStart;
    public GameObject[] PanelModes;
    public Text levelNo;
    public Text textDealResult;
    
    
    // Start is called before the first frame update
    void Start()
    {
        CloseAllPanels();
        ButtonStart.onClick.AddListener(delegate 
        {
            GameStatesControl.instance.ChangeDelayedGameStateTo(GameState.StartLevel, 0);
        });

        RegisterActions();


    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CloseAllPanels()
    {
        PanelIdle.SetActive(false);
        PanelDealing.SetActive(false);
        PanelRobbing.SetActive(false);
        PanelDealDone.SetActive(false);
        PanelDailyReport.SetActive(false);
    }
    //public void GetUiPanel(UiPanelMainType uiPanel)
    //{
    //    CloseAllPanels();
    //    switch (uiPanel)
    //    {
    //        case UiPanelMainType.Start:
    //            {
    //                PanelStart.SetActive(true);
    //                break;
    //            }
    //        case UiPanelMainType.Dealing:
    //            {
    //                PanelDealing.SetActive(true);
    //                break;
    //            }
    //        case UiPanelMainType.DealDone:
    //            {
    //                PanelDealDone.SetActive(true);
    //                break;
    //            }
    //    }
    //}
    public void GetIdlePanel()
    {
        CloseAllPanels();
        PanelIdle.SetActive(true);
    }
    public void GetDealingOrRobbingPanel()
    {
        CloseAllPanels();
        if(GameStatesControl.instance.customerManager.currentCustomerAttributes.customerType == CustomerType.Robber)
        {
            PanelRobbing.SetActive(true);
        }
        else
        {
            PanelDealing.SetActive(true);
        }
        
    }
    public void GetDealDonePanel()
    {
        CloseAllPanels();
        PanelDealDone.SetActive(true);
        switch (GameStatesControl.instance.currentDealResult)
        {
            case DealingResultType.Accepted:
                {
                    textDealResult.text = "" + GameStatesControl.instance.currentDealResult;
                    textDealResult.color = Color.green;
                    break;
                }
            case DealingResultType.Rejected:
                {
                    textDealResult.text = "" + GameStatesControl.instance.currentDealResult;
                    textDealResult.color = Color.red;
                    break;
                }
            default:
                {
                    textDealResult.text = " " ;
                    break;
                }
        }

        
    }
    public void GetDailyReportPanel()
    {
        CloseAllPanels();
        PanelDailyReport.SetActive(true);
    }
    public void RegisterActions()
    {
        OnIdle += GetIdlePanel;
        OnStartLevel += CloseAllPanels;
        OnDealStart += GetDealingOrRobbingPanel;
        OnDealResult += GetDealDonePanel;
        OnDealEnd += CloseAllPanels;
    }
    public void GetRightUiPanelMode(CameraMode camMode)
    {
        switch (camMode)
        {
            case CameraMode.GameCamera:
                {
                    PanelModes[0].SetActive(true);
                    PanelModes[1].SetActive(false);
                    PanelModes[2].SetActive(false);
                    break;
                }
            case CameraMode.PodiumCamera:
                {
                    PanelModes[0].SetActive(false);
                    PanelModes[1].SetActive(true);
                    PanelModes[2].SetActive(false);
                    break;
                }
            case CameraMode.GroomCamera:
                {
                    PanelModes[0].SetActive(false);
                    PanelModes[1].SetActive(false);
                    PanelModes[2].SetActive(true);
                    break;
                }
        }
    }
}
