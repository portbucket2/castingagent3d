﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiManagePodium : MonoBehaviour
{
    public static UiManagePodium instance;
    public GameObject nothingSelectedBoard;
    public GameObject infoBoard;
    public GameObject UpperSearchBoard;
    public GameObject quickSellPanel;
    public GameObject sellPanel;
    public GameObject swipeLeftRightPanel;
    public Button buttonQuickSell;
    public Button buttonSell;
    public Button buttonGroom;
    public Button buttonNext;
    public Button buttonPrevious;
    public Text headerText;

    public Text title         ;
    public Text height        ;
    public Text weight        ;
    public Text skinTone      ;
    public Text dealValue     ;
    public Text estimatedWorth;
    public Text buyerOfferPrice;

    public Toggle[] requirementToggles;
    public Text[] requirementToggleLabelsTexts;


    public PodiumUiMode podiumUiMode;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UiPodiumGetRightPanels(ModelStandingBaseEmptyness modelStandEmp)
    {
        
        switch (podiumUiMode)
        {
            
            case PodiumUiMode.InfoPanelMode:
                {
                    switch (modelStandEmp)
                    {
                        case ModelStandingBaseEmptyness.Empty:
                            {
                                nothingSelectedBoard.SetActive(true);
                                infoBoard.SetActive(false);
                                UpperSearchBoard.SetActive(false);
                                break;
                            }
                        case ModelStandingBaseEmptyness.HasModel:
                            {
                                nothingSelectedBoard.SetActive(false);
                                infoBoard.SetActive(true);
                                buttonQuickSell.gameObject.SetActive(true);
                                buttonSell.gameObject.SetActive(false);
                                UpperSearchBoard.SetActive(false);
                                break;
                                
                            }
                    }

                    break;
                }
            case PodiumUiMode.SearchMode:
                {
                    switch (modelStandEmp)
                    {
                        case ModelStandingBaseEmptyness.Empty:
                            {
                                nothingSelectedBoard.SetActive(true);
                                infoBoard.SetActive(false);
                                UpperSearchBoard.SetActive(false);
                                UpperSearchBoard.SetActive(true);
                                break;
                            }
                        case ModelStandingBaseEmptyness.HasModel:
                            {
                                nothingSelectedBoard.SetActive(false);
                                infoBoard.SetActive(true);
                                buttonQuickSell.gameObject.SetActive(false);
                                buttonSell.gameObject.SetActive(true);
                                UpperSearchBoard.SetActive(true);
                                break;

                            }
                    }
                    
                    break;
                }
        }
        //UiPodiumTextValuesUpdate();
    }
    public void SetPodiumUiMode(PodiumUiMode podiumUiMod)
    {
         podiumUiMode = podiumUiMod;
    }
    public void EstimatedWorthIncrease(AvatarAttributes avatarAttributes , float percentage)
    {
        int estWorth = avatarAttributes.estimatedWorth;
        int newEstWorth =(int) ((float)estWorth *(1 + (percentage / 100)));
        avatarAttributes.estimatedWorth = newEstWorth;
    }
    public void IncreaseWorth()
    {
        EstimatedWorthIncrease(PodiumSystem.instance.modelAvatarScripts[PodiumSystem.instance.currentSnappedShelfIndex], 20);
    }
    public void UiPodiumTextValuesUpdate()
    {
        ModelStandingBaseEmptyness modelStandEmp = ModelStandingBaseEmptyness.Empty;
        if (PodiumSystem.instance.podiumOpeningScenerio == PodiumOpeningScenerio.SearchAModel)
        {
            SetPodiumUiMode(PodiumUiMode.SearchMode);
        }
        else
        {
            SetPodiumUiMode(PodiumUiMode.InfoPanelMode);
        }

        if (PodiumSystem.instance.modelAvatarScripts.Count >=PodiumSystem.instance.currentSnappedShelfIndex+1)
        {

            modelStandEmp = ModelStandingBaseEmptyness.HasModel;

            AvatarAttributes avatarAttributes = PodiumSystem.instance.modelAvatarScripts[PodiumSystem.instance.currentSnappedShelfIndex];
            if(avatarAttributes.AvatarFullyGroomedCheck())
            {
                buttonGroom.interactable = false;
            }
            else
            {
                buttonGroom.interactable =true;
            }

            title.text = "" + avatarAttributes.title;
            height        .text = "" + avatarAttributes.height        ;
            weight        .text = "" + avatarAttributes.weight        ;
            skinTone      .text = "" + avatarAttributes.skinTone      ;
            dealValue     .text = "$ " + avatarAttributes.dealValue     ;
            estimatedWorth.text = "$ " + avatarAttributes.estimatedWorth;
            buyerOfferPrice.text = "$ " + avatarAttributes.estimatedWorth;

        }
        else
        {
            modelStandEmp = ModelStandingBaseEmptyness.Empty;
        }
        switch (podiumUiMode)
        {
            
            case PodiumUiMode.InfoPanelMode:
                {
                    switch (modelStandEmp)
                    {
                        case ModelStandingBaseEmptyness.Empty:
                            {
                                headerText.text = " N / A";
                                buttonNext.gameObject.SetActive(false);
                                buttonPrevious.gameObject.SetActive(false);
                                break;
                            }
                        case ModelStandingBaseEmptyness.HasModel:
                            {
                                headerText.text = " Details ";
                                buttonNext.gameObject.SetActive(false);
                                buttonPrevious.gameObject.SetActive(false);
                                break;

                            }
                    }
                    
                    break;
                }
            case PodiumUiMode.SearchMode:
                {
                    headerText.text = "Search Result";
                    buttonNext.gameObject.SetActive(true);
                    buttonPrevious.gameObject.SetActive(true);
                    UpperSearchBoradInfoUpdate();
                    break;
                }
        }

        UiPodiumGetRightPanels(modelStandEmp);
        //UpperSearchBoradInfoUpdate();
    }

    public void UpperSearchBoradInfoUpdate()
    {
        if(GameStatesControl.instance.customerManager.currentCustomerAttributes.customerType != CustomerType.Director)
        {
            return;
        }
        int requirementCont = 0;
        List<string> reqName = new List<string>();
        List<bool> fulfilled = new List<bool>();
        CustomerAttributes cusAttr = GameStatesControl.instance.customerManager.currentCustomerAttributes;
        AvatarAttributes podiumAvatarAttr;

        int got = 0;
        if (PodiumSystem.instance.currentSnappedShelfIndex < PodiumSystem.instance.modelAvatarScripts.Count)
        {
            podiumAvatarAttr = PodiumSystem.instance.modelAvatarScripts[PodiumSystem.instance.currentSnappedShelfIndex];
            got = 1;
            Debug.Log("GotPodiumChar "+ podiumAvatarAttr.name);
        }
        else
        {
            podiumAvatarAttr = null;
        }
        
        if (cusAttr.heightRequirement != Height.Average)
        {
            requirementCont += 1;
            reqName.Add( cusAttr.heightRequirement.ToString());

            if(podiumAvatarAttr != null)
            {
                if (podiumAvatarAttr.height == cusAttr.heightRequirement)
                {
                    fulfilled.Add(true);
                }
                else
                {
                    fulfilled.Add(false);
                }
            }
            
            else
            {
                fulfilled.Add(false);
            }
        }
        if (cusAttr.weightRequirement != Weight.Average)
        {
            requirementCont += 1;
            reqName.Add(cusAttr.weightRequirement.ToString());
            if (podiumAvatarAttr != null)
            {
            
                if (podiumAvatarAttr.weight == cusAttr.weightRequirement)
                {
                    fulfilled.Add(true);
                }
                else
                {
                    fulfilled.Add(false);
                }
            }
            
            else
            {
                fulfilled.Add(false);
            }
        }
        if (cusAttr.skinToneRequirement != SkinTone.Average)
        {
            requirementCont += 1;
            reqName.Add(cusAttr.skinToneRequirement.ToString());
            if (podiumAvatarAttr != null)
            {
                if (podiumAvatarAttr.skinTone == cusAttr.skinToneRequirement)
                {
                    fulfilled.Add(true);
                }
                else
                {
                    fulfilled.Add(false);
                }
            }
            
            else
            {
                fulfilled.Add(false);
            }
        }

        for (int i = 0; i < requirementToggles. Length; i++)
        {
            requirementToggles[i].gameObject.SetActive(false);
        }
        for (int i = 0; i < reqName.Count; i++)
        {
            requirementToggles[i].gameObject.SetActive(true);
            requirementToggles[i].isOn = fulfilled[i];
            requirementToggleLabelsTexts[i].text = reqName[i];
        }

        
    }

    public void NextSearchedModelSelect()
    {
        int currentShelf = PodiumSystem.instance.currentSnappedShelfIndex;
        int serialInSearchedList = -1;
        for (int i = 0; i < PodiumSystem.instance.searchedModelIndexList.Count; i++)
        {
            if(currentShelf == PodiumSystem.instance.searchedModelIndexList[i])
            {
                serialInSearchedList = i;
            }
        }
        if(serialInSearchedList >= 0)
        {
            if(PodiumSystem.instance.searchedModelIndexList.Count > (serialInSearchedList + 1))
            {
                PodiumSystem.instance.pawnShopShelfSliding.GoSnapToShelf(PodiumSystem.instance.searchedModelIndexList[serialInSearchedList+1]);
            }
        }
    }
    public void PreviousSearchedModelSelect()
    {
        int currentShelf = PodiumSystem.instance.currentSnappedShelfIndex;
        int serialInSearchedList = -1;
        for (int i = 0; i < PodiumSystem.instance.searchedModelIndexList.Count; i++)
        {
            if (currentShelf == PodiumSystem.instance.searchedModelIndexList[i])
            {
                serialInSearchedList = i;
            }
        }
        if (serialInSearchedList > 0)
        {
            PodiumSystem.instance.pawnShopShelfSliding.GoSnapToShelf(PodiumSystem.instance.searchedModelIndexList[serialInSearchedList - 1]);
        }
    }
    public void QuickSellPanelAppear()
    {
        AvatarAttributes avatarAttributes = PodiumSystem.instance.modelAvatarScripts[PodiumSystem.instance.currentSnappedShelfIndex];
        
        quickSellPanel.SetActive(true);
        PodiumSystem.instance.pawnShopShelfSliding.shelfTouchInputs.TouchBlockInPodium();
        QuickSellPanelControl.instance.InfoUpdateQuickSell(avatarAttributes);
    }
    public void QuickSellPanelDisAppear()
    {
        quickSellPanel.SetActive(false);
        PodiumSystem.instance.pawnShopShelfSliding.shelfTouchInputs.TouchUnBlockInPodium();
    }
    public void SellPanelAppear()
    {
        sellPanel.SetActive(true);
        CustomerAttributes cusAttr = GameStatesControl.instance.customerManager.currentCustomerAttributes;
        AvatarAttributes podiumAvatarAttr = PodiumSystem.instance.modelAvatarScripts[PodiumSystem.instance.currentSnappedShelfIndex];
        SellPanelControl.instance.SellPanelTogglesUpdate(requirementToggles, requirementToggleLabelsTexts, podiumAvatarAttr, cusAttr);
        //PodiumSystem.instance.pawnShopShelfSliding.shelfTouchInputs.TouchUnBlockInPodium();
    }
    public void SellPanelDisAppear()
    {
        sellPanel.SetActive(false);
        //PodiumSystem.instance.pawnShopShelfSliding.shelfTouchInputs.TouchUnBlockInPodium();
    }

}
