﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreadmillTool : GroomingTool
{
    public static TreadmillTool instance;

    public bool avatarPosTransition;
    public Vector3 avatarTargetPos;
    public Quaternion avatarTargetRot;

    public GameObject runningPlaceRef;
    
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        DisappearTool();
    }

    // Update is called once per frame
    void Update()
    {
        TravellingActivityFun();
        if (avatarPosTransition)
        {
            avatarOnIt.transform.position = Vector3.MoveTowards(avatarOnIt.transform.position, avatarTargetPos, Time.deltaTime * 7f);
            avatarOnIt.transform.rotation= Quaternion.Lerp(avatarOnIt.transform.rotation, avatarTargetRot, Time.deltaTime * 6f);
            if (avatarOnIt.transform.position== avatarTargetPos){
                avatarPosTransition = false;
                avatarOnIt.transform.rotation = avatarTargetRot;
            }
        }
    }

    public void TakeAvatarOnTreadmil()
    {
        if (this.avatarOnIt != null)
        {
            avatarRootPos = avatarOnIt.transform.position;
            avatarRootRot = avatarOnIt.transform.rotation;

            avatarTargetPos = runningPlaceRef.transform.position;
            avatarTargetRot = runningPlaceRef.transform.rotation;

            avatarPosTransition = true;
        }
            
    }
    public void ReturnAvatarToRoot()
    {
        if(avatarOnIt != null)
        {
            avatarTargetPos = avatarRootPos;
            avatarTargetRot = avatarRootRot;
            //avatarOnIt.transform.rotation = avatarRootRot;

            avatarPosTransition = true;
        }
        
        //avatarOnIt = null;
    }
    override
    public void ActionOnAppear()
    {
        
        //TakeAvatarOnTreadmil();
    }
    override
     public void ActionOnDisAppear()
    {

        //ReturnAvatarToRoot();
    }
}
