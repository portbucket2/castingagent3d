﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakeupTool : GroomingTool
{
    public static MakeupTool instance;
    public GameObject MakeUpBox;
    public Animator makeUpBoxAnim;

    public bool kajolLDone;
    public bool kajolRDone;
    public bool lipstickDone;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        DisappearTool();
    }

    // Update is called once per frame
    void Update()
    {
        TravellingActivityFun();
        if(GroomingSystem.instance.groomingMode == GroomingMode.MakeUp)
        {
            MakeUpBoxAnimsPlay(GroomingSystem.instance.groomCompletionVal);
        }
        
    }
    public void MakeUpBoxAnimsPlay(float progresiion)
    {
        if (Input.GetMouseButton(0))
        {
            if(avatarOnIt != null)
            {
                MakeUpBox.transform.position = avatarOnIt.GetComponent<AvatarAttributes>().makeUpboxHolder.position;
            }
            
            if (progresiion < 20)
            {
                makeUpBoxAnim.SetBool("brushL", true);
                makeUpBoxAnim.SetBool("brushR", false);
            }
            else if (progresiion >= 20 && progresiion < 40)
            {
                makeUpBoxAnim.SetBool("brushL", false);
                makeUpBoxAnim.SetBool("brushR", true);
            }
            else if (progresiion >= 40 && progresiion < 60)
            {
                if (!lipstickDone)
                {
                    makeUpBoxAnim.SetTrigger("lipstick");
                    avatarOnIt.GetComponent<AvatarShadersControl>().LerpToGroomedLook(avatarOnIt.GetComponent<AvatarShadersControl>().lipstickMat, 1);
                    avatarOnIt.GetComponent<AvatarShadersControl>().LerpToGroomedLook(avatarOnIt.GetComponent<AvatarShadersControl>().headMat, 1f);
                    lipstickDone = true;
                }
                
            }
            else if (progresiion >= 60 && progresiion < 80)
            {
                if (!kajolLDone)
                {
                    kajolLDone = true;
                    makeUpBoxAnim.SetTrigger("kajolL");
                    avatarOnIt.GetComponent<AvatarShadersControl>(). LerpToGroomedLook(avatarOnIt.GetComponent<AvatarShadersControl>().kajolMat, 1, 0f,0.5f);
                }
            }
            else if (progresiion >= 80 && progresiion <= 100)
            {
                if (!kajolRDone)
                {
                    kajolRDone = true;
                    makeUpBoxAnim.SetTrigger("kajolR");
                    avatarOnIt.GetComponent<AvatarShadersControl>().LerpToGroomedLook(avatarOnIt.GetComponent<AvatarShadersControl>().kajolMat, 1, 0.5f, 1f);
                }
            }
            else
            {
                makeUpBoxAnim.SetBool("brushL", false);
                makeUpBoxAnim.SetBool("brushR", false);
            }
        }
        else
        {
            makeUpBoxAnim.SetBool("brushL", false);
            makeUpBoxAnim.SetBool("brushR", false);
        }
        
        
    }
    public void MakeUpBoxAnimReset()
    {
        kajolLDone = false;
        kajolRDone = false;
        lipstickDone = false;
        
    }
    public void MakeUpAvatarOnItAssign(GameObject avatar)
    {
        avatarOnIt = avatar;
        
    }
    public void MakeUpDone()
    {
        avatarOnIt.GetComponent<AvatarShadersControl>().LerpToGroomedLook(avatarOnIt.GetComponent<AvatarShadersControl>().bodyMat, 1f);
        avatarOnIt.GetComponent<AvatarShadersControl>().LerpToGroomedLook(avatarOnIt.GetComponent<AvatarShadersControl>().hairMat, 1f);
    }
}
