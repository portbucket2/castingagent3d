﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DollarForDeal : PaperOfDeal
{
    public static DollarForDeal instance;
    public GameObject dollarObj;
    bool scaling;
    Vector3 targetScale;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        PaperInstantSnapTo(customerMangerRootPos);
    }

    // Update is called once per frame
    void Update()
    {
        if (snapping)
        {
            transform.position = Vector3.Lerp(transform.position, targetTrans.position, Time.deltaTime * speed * 5);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetTrans.rotation, Time.deltaTime * speed * 5);
            if (Vector3.Distance(transform.position, targetTrans.position) < 0.01f)
            {
                snapping = false;
                transform.position = targetTrans.position;
                transform.rotation = targetTrans.rotation;
            }

            timeNeeded += Time.deltaTime;
        }

        if (scaling)
        {
            dollarObj.transform.localScale = Vector3.MoveTowards(dollarObj.transform.localScale, targetScale, Time.deltaTime * 3);
            if(dollarObj.transform.localScale== targetScale)
            {
                scaling = false;
            }
        }
    }
    public void DollarAppearScaled()
    {
        targetScale = new Vector3(1, 1, 1);
        scaling = true;
    }
    public void DollarDisAppearScaled()
    {
        targetScale = new Vector3(0, 0, 0);
        scaling = true;
    }
}
