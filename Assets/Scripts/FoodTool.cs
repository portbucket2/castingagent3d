﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodTool : GroomingTool
{
    public static FoodTool instance;
    [Header("Food")]
    public GameObject burgerObj;
    public Vector3    burgerRootPos;
    public Quaternion burgerRootRotation;
    public bool burgerSnap;
    public Transform burgerTargetTransform;

    public GameObject cokeObj;
    public Vector3    cokeRootPos;
    public Quaternion cokeRootRotation;
    public bool       cokeSnap;
    public Transform  cokeTargetTransform;
    public Transform cokeRefPos;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        DisappearTool();
        burgerRootPos     = burgerObj.transform.localPosition ;
        burgerRootRotation= burgerObj.transform.localRotation;

        cokeRootPos = cokeObj.transform.localPosition;
        cokeRootRotation = cokeObj.transform.localRotation;
    }

    // Update is called once per frame
    void Update()
    {
        TravellingActivityFun();
        if (burgerSnap)
        {
            burgerObj.transform.position = Vector3.MoveTowards(burgerObj.transform.position, burgerTargetTransform.position, Time.deltaTime * 10);
            burgerObj.transform.rotation = Quaternion.Lerp(burgerObj.transform.rotation, burgerTargetTransform.rotation, Time.deltaTime * 5);
            if (burgerObj.transform.position == burgerTargetTransform.position)
            {
                burgerObj.transform.rotation = burgerTargetTransform.rotation;
                burgerSnap = false;

                burgerObj.transform.SetParent(avatarOnIt.GetComponent<AvatarAttributes>().dollarHolder.transform);

            }


        }
        if (cokeSnap)
        {
            cokeObj.transform.position = Vector3.MoveTowards(cokeObj.transform.position, cokeTargetTransform.position, Time.deltaTime * 10);
            cokeObj.transform.rotation = Quaternion.Lerp(cokeObj.transform.rotation, cokeTargetTransform.rotation, Time.deltaTime * 5);
            if (cokeObj.transform.position == cokeTargetTransform.position)
            {
                cokeObj.transform.rotation = cokeTargetTransform.rotation;
                cokeSnap = false;

                if(cokeTargetTransform == avatarOnIt.GetComponent<AvatarAttributes>().cokeHolder.transform){
                    cokeObj.transform.SetParent(avatarOnIt.GetComponent<AvatarAttributes>().cokeHolder.transform);
                }
                

            }


        }

    }

    public void BurgerSnapToHand()
    {
        //avatarOnIt = GroomingSystem.instance.groomAvatar;
        burgerTargetTransform = avatarOnIt.GetComponent<AvatarAttributes>().dollarHolder.transform;
        burgerSnap = true;
        
    }
    public void CokeSnapToHand()
    {
        //avatarOnIt = GroomingSystem.instance.groomAvatar;
        cokeTargetTransform = avatarOnIt.GetComponent<AvatarAttributes>().cokeHolder.transform;
        cokeSnap = true;

    }
    public void BurgerSnapToRoot()
    {
        burgerObj.transform.SetParent(toolObj.transform);
        burgerObj.transform.localPosition= burgerRootPos;
        burgerObj.transform.localRotation = burgerRootRotation;

        burgerObj.SetActive(true);
    }
    public void CokeSnapToRoot()
    {
        cokeObj.transform.SetParent(toolObj.transform);
        cokeTargetTransform = cokeRefPos;
        cokeSnap = true;
        

        cokeObj.SetActive(true);
    }
    public void BurgerDisAppear()
    {
        burgerObj.transform.SetParent(toolObj.transform);
        burgerObj.transform.localPosition = burgerRootPos;
        burgerObj.transform.localRotation = burgerRootRotation;
        burgerObj.SetActive(false);
    }
    public void FoodAvatarOnItAssign(GameObject avatar)
    {
        avatarOnIt = avatar;
    }
    //public void CokeDisAppear()
    //{
    //    cokeObj.transform.SetParent(cokeObj.transform);
    //    cokeObj.transform.localPosition = cokeRootPos;
    //    cokeObj.transform.localRotation = cokeRootRotation;
    //    cokeObj.SetActive(false);
    //}

    override
    public void ActionOnAppear()
    {

        //BurgerSnapToHand();
        //Debug.Log("BURGER");
    }

}
