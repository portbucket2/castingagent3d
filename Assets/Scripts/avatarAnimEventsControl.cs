﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class avatarAnimEventsControl : MonoBehaviour
{
    public Animator anim;
    //public GunHolderControl gunHolderControl;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void TreadMillTakeAvatarOnYou()
    {
        //Debug.Log("TREADME");
        TreadmillTool.instance.TakeAvatarOnTreadmil();
    }
    public void TreadMillREturnAvatar()
    {
        //Debug.Log("TREADME");
        TreadmillTool.instance.ReturnAvatarToRoot();
    }
    public void BurgerOnHand()
    {
        FoodTool.instance.BurgerSnapToHand();
    }
    public void CokeOnHand()
    {
        FoodTool.instance.CokeSnapToHand();
    }
    public void CokeReturn()
    {
        FoodTool.instance.CokeSnapToRoot();
    }
    //public void GunRaise()
    //{
    //    gunHolderControl = GetComponentInChildren<GunHolderControl>();
    //    if (gunHolderControl != null)
    //        gunHolderControl.GunInHandAppear();
    //}
    //public void GunVanish()
    //{
    //    gunHolderControl = GetComponentInChildren<GunHolderControl>();
    //    if (gunHolderControl != null)
    //        gunHolderControl.GunInHandDisAppear();
    //}
}
