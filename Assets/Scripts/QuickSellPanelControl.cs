﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuickSellPanelControl : MonoBehaviour
{
    public static QuickSellPanelControl instance;
    public Text modelTitle;
    public Text modelHeight;
    public Text modelWeight;
    public Text modelSkinTone;
    public Text modelDealPrice;

    public Text OfferPrice;
    public Text profitText;

    public Image modelPP;
    public Image directorPP;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void InfoUpdateQuickSell(AvatarAttributes avatarAttr)
    {
        modelTitle.text = avatarAttr.title;
        modelHeight.text =""+ avatarAttr.height;
        modelWeight.text = "" + avatarAttr.weight;
        modelSkinTone.text = "" + avatarAttr.skinTone;
        modelDealPrice.text = "$ " + avatarAttr.dealValue;

        OfferPrice.text = "$ " + avatarAttr.estimatedWorth;

        profitText.text = "$ " + (avatarAttr.estimatedWorth - avatarAttr.dealValue);

        modelPP.sprite = avatarAttr.charPP;

        //directorPP = ;
        //modelPP = ;
    }
}
