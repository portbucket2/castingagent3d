﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathPoint : GameManagerMain
{
    public Transform nextPoint;
    public PathPointType pathPointType;
    //public CustomerAttributes customerAttributes;
    public Transform acceptedNextPoint;
    public Transform rejectedNextPoint;


    // Start is called before the first frame update
    //void Start()
    //{
    //    
    //}
    //
    //// Update is called once per frame
    //void Update()
    //{
    //    
    //}

    public void PathPointFunction(CustomerAttributes customerAttributes)
    {
        
        if(customerAttributes.targetPos == nextPoint)
        {
            Debug.Log("GOTSAME");
            return;
        }
        //customerAttributes = customerAttributes;
        switch (pathPointType)
        {
            case PathPointType.Ordinary:
                {
                    customerAttributes.targetPos = nextPoint;
                    break;
                }
            case PathPointType.OpenDoorSensor:
                {
                    customerAttributes.targetPos = nextPoint;
                    //Debug.Log("OpenTheDoor");
                    ChangeGameStateTo(GameState.CustomerOnDoor);
                    break;
                }
            case PathPointType.AtDesk:
                {
                    customerAttributes.targetPos = nextPoint;
                    if(acceptedNextPoint)customerAttributes.acceptedTargetPos = acceptedNextPoint;
                    if (rejectedNextPoint) customerAttributes.rejectedTargetPos = rejectedNextPoint;
                    customerAttributes.moving = false;
                    ChangeGameStateTo(GameState.CustomerOnDesk);
                    break;
                }
            case PathPointType.EndPoint:
                {
                    customerAttributes.targetPos = null;
                    customerAttributes.moving = false;
                    //ChangeGameStateTo(GameState.LevelEnd);
                    break;
                }
        }
    }
}
