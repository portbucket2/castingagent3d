﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.faithstudio.SDK;

public class GameStatesControl : MonoBehaviour
{
    public int levelIndex;
    public int levelIndexDisplayed;
    public GameState gameState;
    public GameManagerMain gameManagerMain;

    
    
    public static GameStatesControl instance;
    public CustomerManager customerManager;
    public UiManagerMain uiManagerMain;
    public DealingResultType currentDealResult;
    public LevelEndType levelEndType;
    public bool dayEnd;
    public int levelsPerDay;
    public bool dailyReportDisabled;
    // Start is called before the first frame update
    private void Awake()
    {
        instance = this;
        
    }
    void Start()
    {
        gameManagerMain.ChangeGameStateTo(GameState.Idle);
        
        
        //ChangeDelayedGameStateTo(GameState.CustomerOnDoor, 1);
    }

    

    // Update is called once per frame
    void Update()
    {
        
    }

    public IEnumerator GetStateDelayed(GameState gameStat, float t)
    {
        yield return new WaitForSeconds(t);
        gameManagerMain.ChangeGameStateTo(gameStat);
    }
    public void ChangeDelayedGameStateTo(GameState gameStat, float t)
    {
        StartCoroutine(GetStateDelayed(gameStat, t));
    }
    public void DealResult(DealingResultType dealResult)
    {
        currentDealResult = dealResult;
        customerManager.DealResultOnCustomer(dealResult);
        //gameManagerMain.ChangeGameStateTo(GameState.DealResult);
    }

    public void DealAccept()
    {
        DealResult(DealingResultType.Accepted);
    }
    public void DealReject()
    {
        DealResult(DealingResultType.Rejected);
    }
    public void DealSlapped()
    {
        DealResult(DealingResultType.Slapped);
    }
    public void DealRobbed()
    {
        DealResult(DealingResultType.Robbed);
    }
    public void SetLevelEndType(LevelEndType lvlEndType)
    {
        levelEndType = lvlEndType;
    }
    public void DayEndOrNot()
    {
        float e = (levelIndex + 1) % levelsPerDay;
        if(e == 0)
        {
            dayEnd = true;
        }
        else
        {
            dayEnd = false;
        }
        //specialCaseEdited
        if (dailyReportDisabled)
        {
            dayEnd = false;
        }
    }
    public void ReadyForNextLevelFuntion()
    {
        switch (levelEndType)
        {
            case LevelEndType.ModelAdded:
                {
                    
                    //PodiumSystem.instance.PodiumOpenScenerioFuntion(PodiumOpeningScenerio.ModelAdded);
                    break;
                }
            case LevelEndType.NoModelAdded:
                {
                    if (dayEnd)
                    {
                        uiManagerMain.GetDailyReportPanel();
                    }
                    else
                    {
                        GameStatesControl.instance.NextLevel();
                    }
                    
                    break;
                }
            
        }
    }
    public void LevelEndFuntionOfGameStates()
    {
        switch (levelEndType)
        {
            case LevelEndType.ModelAdded:
                {

                    PodiumSystem.instance.PodiumOpenScenerioFuntion(PodiumOpeningScenerio.ModelAdded);
                    break;
                }
            case LevelEndType.NoModelAdded:
                {

                    break;
                }

        }
    }
    public void NextLevel()
    {
        //levelIndex += 1;
        //if(levelIndex >= customerManager.customers.Length)
        //{
        //    levelIndex = 0;
        //}
        gameManagerMain.ChangeGameStateTo(GameState.StartLevel);

        //DataSavingSystem.instance.LevelIndexSet(levelIndex);
        
    }
    public void LevelIndexIncrease()
    {
        FacebookSdkLevelAccomplishedFun();
        levelIndexDisplayed += 1;
        levelIndex += 1;
        
        if (levelIndex >= customerManager.customers.Length)
        {
            levelIndex = 0;
        }
        DataSavingSystem.instance.LevelIndexSet(levelIndex);
        DataSavingSystem.instance.LevelIndexDisplayedSet(levelIndexDisplayed);

        
    }

    public void FacebookSdkLevelStartFun()
    {
        FacebookAnalyticsManager.Instance.FBALevelStart(levelIndex + 1);
        Debug.Log("Level Started " + (levelIndex + 1));
    }
    public void FacebookSdkLevelAccomplishedFun()
    {
        FacebookAnalyticsManager.Instance.FBALevelComplete(levelIndex + 1);
        Debug.Log("Level Accomplished " + (levelIndex + 1));
    }

}
