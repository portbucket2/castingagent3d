﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerMain : MonoBehaviour
{
    //public int levelIndex;
    
    //public GameState gameState;

    public static event Action OnIdle               ;
    public static event Action OnStartLevel        ;
    public static event Action OnCustomerOnDoor   ;
    public static event Action OnCustomerOnDesk   ;
    public static event Action OnPaperOnTable     ;
    public static event Action OnScanningPaper   ;
    public static event Action OnSayOfferCustomer;
    public static event Action OnDealStart       ;
    public static event Action OnDealResult      ;
    public static event Action OnDealClosingOnTable;
    public static event Action OnDealEnd         ;
    public static event Action OnLevelEnd        ;
    public static event Action OnReadyForNextLevel;

    //public GameStatesControl gameStatesControl;
    // Start is called before theLevelEnd   first frame update
    #region Unity_Funtions
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }
    #endregion Unity_Functions
    #region Action_Calling_Functions
    public void OnIdleActionCall(){OnIdle?.Invoke();}
    
    public void OnStartLevelActionCall()       {OnStartLevel      ?.Invoke();}
    public void OnCustomerOnDoorActionCall()   {OnCustomerOnDoor  ?.Invoke();}
    public void OnCustomerOnDeskActionCall()   {OnCustomerOnDesk  ?.Invoke();}
    public void OnPaperOnTableActionCall()     {OnPaperOnTable    ?.Invoke();}
    public void OnScanningPaperActionCall()    {OnScanningPaper   ?.Invoke();}
    public void OnSayOfferCustomerActionCall() {OnSayOfferCustomer?.Invoke();}
    public void OnDealStartActionCall()        {OnDealStart       ?.Invoke();}
    public void OnDealResultActionCall()       {OnDealResult      ?.Invoke();}
    public void OnDealClosingOnTableActionCall() { OnDealClosingOnTable?.Invoke(); }
    public void OnDealEndActionCall()          {OnDealEnd         ?.Invoke();}
    public void OnLevelEndActionCall()         {OnLevelEnd        ?.Invoke(); }
    public void OnReadyForNextLevelActionCall() {OnReadyForNextLevel?.Invoke(); }
    #endregion Action_Calling_Functions

    #region Virtual_Functions
    public virtual void IdleFuntion()
    {
        GameStatesControl.instance.levelIndex =DataSavingSystem.instance.LevelIndexGet();
        GameStatesControl.instance.levelIndexDisplayed = DataSavingSystem.instance.LevelIndexDisplayedGet();
        AccountManager.instance.balanceCurrent = DataSavingSystem.instance.BalanceurrentGet();

        PodiumSystem.instance.SwitchCameraTo(CameraMode.GameCamera);
        ScreenAttributes.instance.MonitorDisplay(MonitorMode.Idle);

        GameStatesControl.instance.uiManagerMain.levelNo.text = "" + (GameStatesControl.instance.levelIndexDisplayed + 1);
    }
    public virtual void StartLevelFuntion()
    {

        GameStatesControl.instance.DayEndOrNot();
        GameStatesControl.instance.SetLevelEndType(LevelEndType.NoModelAdded);

        GameStatesControl.instance.uiManagerMain.levelNo.text ="" + ( GameStatesControl.instance.levelIndexDisplayed + 1);
        //GameStatesControl.instance.customerManager.currentCustomerAttributes.avatarVisual.GetComponent<AvatarAnimatorController>().Walk();

        GameStatesControl.instance. FacebookSdkLevelStartFun();
    }
    public virtual void CustomerOnDoorFuntion()
    {
        DoorAnimControl.instance.DoorOpen();
    }
    public virtual void CustomerOnDeskFuntion()
    {
        if(GameStatesControl.instance.customerManager.currentCustomerAttributes.customerType == CustomerType.Model)
        {
            GameStatesControl.instance.ChangeDelayedGameStateTo(GameState.PaperOnTable, .5f);
            GameStatesControl.instance.customerManager.currentCustomerAttributes.avatarVisual.GetComponent<AvatarAnimatorController>().PaperOnTable();

        }
        else 
        {
            GameStatesControl.instance.ChangeDelayedGameStateTo(GameState.SayOfferCustomer, 0);
        }
        
    }
    public virtual void PaperOnTableFuntion()
    {
        GameStatesControl.instance.ChangeDelayedGameStateTo(GameState.ScanningPaper, 1);
        PaperOfDeal.instance.PaperSmmothSnapTo(PaperOfDeal.instance.scanPos);
        PaperOfDeal.instance.UnfoldScroll();
    }
    public virtual void ScanningPaperFuntion()
    {
        GameStatesControl.instance.ChangeDelayedGameStateTo(GameState.SayOfferCustomer, 1.5f);
        ScreenAttributes.instance.MonitorDisplay(MonitorMode.Scanning);
        ScannerControl.instance.ScannerParticlePlay();
        //GameStatesControl.instance.customerManager.currentCustomerAttributes.avatarVisual.GetComponent<AvatarAnimatorController>().Idle();
    }
    public virtual void SayOfferCustomerFuntion()
    {
        if(GameStatesControl.instance.customerManager.currentCustomerAttributes.customerType == CustomerType.Model)
        {
            ScreenAttributes.instance.MonitorInfoUpdateNew(GameStatesControl.instance.customerManager.currentCustomerAttributes);
            ScreenAttributes.instance.MonitorDisplay(MonitorMode.ItemOnfoNormal);
        }
        else if (GameStatesControl.instance.customerManager.currentCustomerAttributes.customerType == CustomerType.Robber)
        {
            GunHolderControl gunHolderControl = GameStatesControl.instance.customerManager.currentCustomerAttributes.avatarVisual.GetComponentInChildren<GunHolderControl>();
            if (gunHolderControl != null)
                gunHolderControl.GunInHandAppear();
        }
        
        GameStatesControl.instance.ChangeDelayedGameStateTo(GameState.DealStart, 1);
        GameStatesControl.instance.customerManager.currentCustomerAttributes.avatarVisual.GetComponent<AvatarAnimatorController>().SayPrice();
    }
    public virtual void DealStartFuntion()
    {
        
    }
    public virtual void DealResultFuntion()
    {
        if( GameStatesControl.instance.currentDealResult == DealingResultType.Accepted)
        {
            ParticleControlSystem.instance.ParticlePlay(ParticleControlSystem.instance.confettiParticle);
        }
        
        GameStatesControl.instance.ChangeDelayedGameStateTo(GameState.DealClosingOntable, 2f);
       
    }
    public virtual void DealClosingOnTableFunction()
    {
        if (GameStatesControl.instance.customerManager.currentCustomerAttributes.customerType == CustomerType.Robber)
        {
            GunHolderControl gunHolderControl = GameStatesControl.instance.customerManager.currentCustomerAttributes.avatarVisual.GetComponentInChildren<GunHolderControl>();
            if (gunHolderControl != null)
                gunHolderControl.GunInHandDisAppear();
        }
        GameStatesControl.instance.ChangeDelayedGameStateTo(GameState.DealEnd, 1);
        ScreenAttributes.instance.MonitorDisplay(MonitorMode.Idle);
    }
    public virtual void DealEndFuntion()
    {
        GameStatesControl.instance.ChangeDelayedGameStateTo(GameState.LevelEnd, 1);
    }
    public virtual void LevelEndFuntion()
    {
        GameStatesControl.instance.ChangeDelayedGameStateTo(GameState.ReadyForNextLevel, 0f);
    }
    public virtual void ReadyForNextLevelFuntion()
    {
        //GameStatesControl.instance.NextLevel();
    }


    #endregion Virtual_Functions

    #region Custom_Funtions
    public virtual void ChangeGameStateTo(GameState gameStat)
    {

        GameStatesControl.instance.gameState = gameStat;
        Debug.Log("Called " + gameStat +" "+ GameStatesControl.instance.gameState);

        switch (GameStatesControl.instance.gameState)
        {
            case GameState.Idle:
                {
                    IdleFuntion();
                    OnIdleActionCall();
                    break;
                }
            case GameState.StartLevel:
                {
                    StartLevelFuntion();
                    OnStartLevelActionCall();
                    break;
                }
            case GameState.CustomerOnDoor:
                {
                    CustomerOnDoorFuntion();
                    OnCustomerOnDoorActionCall();
                    break;
                }
            case GameState.CustomerOnDesk:
                {
                    CustomerOnDeskFuntion();
                    OnCustomerOnDeskActionCall();
                    break;
                }
            case GameState.PaperOnTable:
                {
                    PaperOnTableFuntion();
                    OnPaperOnTableActionCall();
                    break;
                }
            case GameState.ScanningPaper:
                {
                    ScanningPaperFuntion();
                    OnScanningPaperActionCall();
                    break;
                }
            case GameState.SayOfferCustomer:
                {
                    SayOfferCustomerFuntion();
                    OnSayOfferCustomerActionCall();
                    break;
                }
            case GameState.DealStart:
                {
                    DealStartFuntion();
                    OnDealStartActionCall();
                    break;
                }
            case GameState.DealResult:
                {
                    DealResultFuntion();
                    OnDealResultActionCall();
                    break;
                }
            case GameState.DealClosingOntable:
                {
                    DealClosingOnTableFunction();
                    OnDealClosingOnTableActionCall();
                    break;
                }
            case GameState.DealEnd:
                {
                    DealEndFuntion();
                    OnDealEndActionCall();
                    break;
                }
            case GameState.LevelEnd:
                {
                    LevelEndFuntion();
                    OnLevelEndActionCall();
                    break;
                }
            case GameState.ReadyForNextLevel:
                {
                    ReadyForNextLevelFuntion();
                    OnReadyForNextLevelActionCall();
                    break;
                }
        }
    }
    #endregion Custom_Funtions
}
