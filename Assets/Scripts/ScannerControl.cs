﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScannerControl : MonoBehaviour
{
    public static ScannerControl instance;
    public ParticleSystem scannerParticle;
    public Animator sneakerAnim;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ScannerParticlePlay()
    {
        scannerParticle.Play();
    }

    public void SneakerThrow()
    {
        if(sneakerAnim != null)
        sneakerAnim.SetTrigger("throw");
        ParticleControlSystem.instance.ParticlePlay(ParticleControlSystem.instance.SlapParticle , 0.3f);
    }
}
