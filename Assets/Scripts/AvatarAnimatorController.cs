﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvatarAnimatorController : MonoBehaviour
{
    public Animator avatarAnim;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Walk()
    {
        if(avatarAnim != null)
        avatarAnim.SetTrigger("walk");
    }
    public void PaperOnTable()
    {
        if (avatarAnim != null)
            avatarAnim.SetTrigger("paperOnTable");
    }
    public void SayPrice()
    {
        if (avatarAnim != null)
            avatarAnim.SetTrigger("sayPrice");
    }
    public void Accepted()
    {
        if (avatarAnim != null)
            avatarAnim.SetTrigger("accepted");
    }
    public void Rejected()
    {
        if (avatarAnim != null)
            avatarAnim.SetTrigger("rejected");
    }
    public void Idle()
    {
        if (avatarAnim != null)
            avatarAnim.SetTrigger("idle");
    }
    public void WalkSad()
    {
        if (avatarAnim != null)
            avatarAnim.SetTrigger("walkSad");
    }
    public void TreadMilUp()
    {
        if (avatarAnim != null)
            avatarAnim.SetTrigger("treadMilUp");
    }
    public void TreadMilDown()
    {
        if (avatarAnim != null)
            avatarAnim.SetTrigger("treadMilDown");
    }
    public void JoggingOn()
    {
        if (avatarAnim != null)
            avatarAnim.SetBool("jogging", true);
    }
    public void JoggingOff()
    {
        if (avatarAnim != null)
            avatarAnim.SetBool("jogging",false);
    }
    public void Sit()
    {
        if (avatarAnim != null)
            avatarAnim.SetBool("sit", true);
    }
    public void StandUp()
    {
        if (avatarAnim != null)
            avatarAnim.SetBool("sit", false);
    }
    public void OutfitChanged()
    {
        if (avatarAnim != null)
            avatarAnim.SetTrigger("outfitChanged");
    }
    public void BurgerTake()
    {
        if (avatarAnim != null)
            avatarAnim.SetTrigger("burgerTake");
    }
    public void Eat()
    {
        if (avatarAnim != null)
            avatarAnim.SetBool("eat", true);
    }
    public void EatStop()
    {
        if (avatarAnim != null)
            avatarAnim.SetBool("eat",false);
    }
    public void DrinkTake()
    {
        if (avatarAnim != null)
            avatarAnim.SetTrigger("drinkTake");
    }
    public void Drink()
    {
        if (avatarAnim != null)
            avatarAnim.SetBool("drink", true);
    }
    public void DrinkStop()
    {
        if (avatarAnim != null)
            avatarAnim.SetBool("drink", false);
    }
    public void FoodDone()
    {
        if (avatarAnim != null)
            avatarAnim.SetTrigger("foodDone");
    }
    public void Reaction()
    {
        if (avatarAnim != null)
            avatarAnim.SetTrigger("reaction");
    }
}
