﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiManageGrooming : MonoBehaviour
{
    public GameObject groomMenuPanel;
    public GameObject groomGymPanel;
    public GameObject groomFoodPanel;
    public GameObject groomMakeUpPanel;
    public GameObject groomOutfitPanel;

    public Slider[] groomSliders;
    public GameObject[] preGroomingPanels;
    public GameObject[] activityGroomingPanels;
    public GameObject[] doneGroomingPanels;
    public GameObject[] reactionGroomingPanels;
    public GameObject[] postGroomingPanels;
    public float sliderValue;
    public static UiManageGrooming instance;

    public Button ButtonGym   ;
    public Button ButtonFood  ;
    public Button ButtonMakeUp;
    public Button ButtonOutfit;

    public bool groomBarFull;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(GroomingSystem.instance.groomingMode != GroomingMode.Menu && !groomBarFull)
        {
            groomSliders[(int)GroomingSystem.instance.groomingMode - 1].value =(float) (GroomingSystem.instance.groomCompletionVal/100f);
            if(groomSliders[(int)GroomingSystem.instance.groomingMode - 1].value >= 1)
            {
                groomBarFull = true;
                //ReactionGroomingPanelAppear();
                DoneGroomingPanelAppear();
            }
        }
        
    }
    public void GroomingUiPanelsUpdateAsGroomingMode()
    {
        CloseAllGroomPanels();
        GroomingMode groomMode = GroomingSystem.instance.groomingMode;
        
        switch (groomMode)
        {
            case GroomingMode.Menu:
                {
                    groomMenuPanel.SetActive(true);
                    ManageGroomButtons();
                    GroomingSystem.instance.groomingStage = GroomingStage.NotStartedYet;
                    break;
                }
            case GroomingMode.Gym:
                {
                    groomGymPanel.SetActive(true);
                    break;
                }
            case GroomingMode.Eating:
                {
                    groomFoodPanel.SetActive(true);
                    break;
                }
            case GroomingMode.MakeUp:
                {
                    groomMakeUpPanel.SetActive(true);
                    break;
                }
            case GroomingMode.Outfit:
                {
                    groomOutfitPanel.SetActive(true);
                    break;
                }
        }
        PreGroomingPanelAppear();
        
    }
    public void ManageGroomButtons()
    {
        if (GroomingSystem.instance.groomAvatarAttr != null)
        {
            AvatarAttributes groomAvatarAtt = GroomingSystem.instance.groomAvatarAttr;
            Weight weightAtt = groomAvatarAtt.weight;
            switch (weightAtt)
            {
                case Weight.Skinny:
                    {
                        ButtonGym.gameObject.SetActive(false);
                        ButtonFood.gameObject.SetActive(true);

                        ButtonFood.interactable = true;
                        break;
                    }
                case Weight.Healthy:
                    {
                        ButtonGym.gameObject.SetActive(true);
                        ButtonFood.gameObject.SetActive(false);

                        ButtonGym.interactable = true;
                        break;
                    }
                default:
                    {
                        ButtonGym.interactable = false;
                        ButtonFood.interactable = false;
                        

                        break;
                    }

            }
            switch (groomAvatarAtt.makeUpState)
            {
                case MakeUpState.Ordinary:
                    {
                        ButtonMakeUp.interactable = true;
                        break;
                    }
                case MakeUpState.MakeUpDone:
                    {
                        ButtonMakeUp.interactable = false;
                        break;
                    }
            }
            switch (groomAvatarAtt.outfitState)
            {
                case OutfitState.Ordinary:
                    {
                        ButtonOutfit.interactable = true;
                        break;
                    }
                case OutfitState.OutfitUpdated:
                    {
                        ButtonOutfit.interactable = false;
                        break;
                    }
            }


        }
    }
    public void CloseAllGroomPanels()
    {
        groomMenuPanel  .SetActive(false);
        groomGymPanel   .SetActive(false);
        groomFoodPanel  .SetActive(false);
        groomMakeUpPanel.SetActive(false);
        groomOutfitPanel.SetActive(false);
    }
    public void PreGroomingPanelAppear()
    {
        GroomingMode groomMode = GroomingSystem.instance.groomingMode;
        int index = 0;
        if (GroomingSystem.instance.groomingMode != GroomingMode.Menu)
        {
            index = (int)GroomingSystem.instance.groomingMode - 1;

            preGroomingPanels[index].SetActive(true);
            activityGroomingPanels[index].SetActive(false);
            doneGroomingPanels[index].SetActive(false);
            reactionGroomingPanels[index].SetActive(false);
            postGroomingPanels[index].SetActive(false);
            
            switch (groomMode)
            {
                case GroomingMode.Menu:
                    {
                       
                        break;
                    }
                case GroomingMode.Gym:
                    {
                        
                        break;
                    }
                case GroomingMode.Eating:
                    {
                        
                        break;
                    }
                case GroomingMode.MakeUp:
                    {
                        
                        break;
                    }
                case GroomingMode.Outfit:
                    {
                        
                        break;
                    }
            }
            GroomingSystem.instance.groomingStage = GroomingStage.PreGrooming;

            GroomingSystem.instance.GroomPreActivityFuntion();
            StartCoroutine(ActivityPanelAppearCoroutime(1.0f));
            groomBarFull = false;
        }
        
    }
    public void ActivityGroomingPanelAppear()
    {
        GroomingMode groomMode = GroomingSystem.instance.groomingMode;
        int index = 0;
        if (GroomingSystem.instance.groomingMode != GroomingMode.Menu)
        {
            index = (int)GroomingSystem.instance.groomingMode - 1;

            preGroomingPanels[index].SetActive(false);
            activityGroomingPanels[index].SetActive(true);
            doneGroomingPanels[index].SetActive(false);
            reactionGroomingPanels[index].SetActive(false);
            postGroomingPanels[index].SetActive(false);
            switch (groomMode)
            {
                case GroomingMode.Menu:
                    {

                        break;
                    }
                case GroomingMode.Gym:
                    {

                        break;
                    }
                case GroomingMode.Eating:
                    {

                        break;
                    }
                case GroomingMode.MakeUp:
                    {

                        break;
                    }
                case GroomingMode.Outfit:
                    {

                        break;
                    }
            }
            GroomingSystem.instance.groomingStage = GroomingStage.ActiveGromming;
        }
        
    }
    public void DoneGroomingPanelAppear()
    {
        GroomingMode groomMode = GroomingSystem.instance.groomingMode;
        int index = 0;
        if (GroomingSystem.instance.groomingMode != GroomingMode.Menu)
        {
            index = (int)GroomingSystem.instance.groomingMode - 1;

            preGroomingPanels[index].SetActive(false);
            activityGroomingPanels[index].SetActive(false);
            doneGroomingPanels[index].SetActive(true);
            reactionGroomingPanels[index].SetActive(false);
            postGroomingPanels[index].SetActive(false);
            switch (groomMode)
            {
                case GroomingMode.Menu:
                    {

                        break;
                    }
                case GroomingMode.Gym:
                    {

                        break;
                    }
                case GroomingMode.Eating:
                    {

                        break;
                    }
                case GroomingMode.MakeUp:
                    {

                        break;
                    }
                case GroomingMode.Outfit:
                    {

                        break;
                    }
            }
            GroomingSystem.instance.groomingStage = GroomingStage.DoneGrooming;
            StartCoroutine(ReactionGroomingPanelAppearCoroutime(1.0f));

            GroomingSystem.instance.DoneGroomingActivityFuntion();
            UiManagePodium.instance. EstimatedWorthIncrease(GroomingSystem.instance.groomAvatarAttr, 50);
        }

    }
    public void ReactionGroomingPanelAppear()
    {
        GroomingMode groomMode = GroomingSystem.instance.groomingMode;
        int index = 0;
        if (GroomingSystem.instance.groomingMode != GroomingMode.Menu)
        {
            index = (int)GroomingSystem.instance.groomingMode - 1;

            preGroomingPanels[index].SetActive(false);
            activityGroomingPanels[index].SetActive(false);
            doneGroomingPanels[index].SetActive(false);
            reactionGroomingPanels[index].SetActive(true);
            postGroomingPanels[index].SetActive(false);
            switch (groomMode)
            {
                case GroomingMode.Menu:
                    {

                        break;
                    }
                case GroomingMode.Gym:
                    {

                        break;
                    }
                case GroomingMode.Eating:
                    {

                        break;
                    }
                case GroomingMode.MakeUp:
                    {

                        break;
                    }
                case GroomingMode.Outfit:
                    {

                        break;
                    }
            }
            GroomingSystem.instance.groomingStage = GroomingStage.ReactionOfGrooming;
            StartCoroutine(PostGroomingPanelAppearCoroutime(4));

            GroomingSystem.instance.GroomReactionActivityFuntion();
        }

    }
    public void PostGroomingPanelAppear()
    {
        GroomingMode groomMode = GroomingSystem.instance.groomingMode;
        int index = 0;
        if (GroomingSystem.instance.groomingMode != GroomingMode.Menu)
        {
            index = (int)GroomingSystem.instance.groomingMode - 1;

            preGroomingPanels[index].SetActive(false);
            activityGroomingPanels[index].SetActive(false);
            doneGroomingPanels[index].SetActive(false);
            reactionGroomingPanels[index].SetActive(false);
            postGroomingPanels[index].SetActive(true);
            switch (groomMode)
            {
                case GroomingMode.Menu:
                    {

                        break;
                    }
                case GroomingMode.Gym:
                    {

                        break;
                    }
                case GroomingMode.Eating:
                    {

                        break;
                    }
                case GroomingMode.MakeUp:
                    {

                        break;
                    }
                case GroomingMode.Outfit:
                    {

                        break;
                    }
            }
            GroomingSystem.instance.groomingStage = GroomingStage.PostGrooming;
            GroomingSystem.instance.GroomingDoneFuntion();

            GroomingSystem.instance.GroomPostActivityFuntion();
        }
        
    }

    IEnumerator ActivityPanelAppearCoroutime(float t)
    {
        yield return new WaitForSeconds(t);
        ActivityGroomingPanelAppear();
    }
    IEnumerator PostGroomingPanelAppearCoroutime(float t)
    {
        yield return new WaitForSeconds(t);
        PostGroomingPanelAppear();
    }
    IEnumerator ReactionGroomingPanelAppearCoroutime(float t)
    {
        yield return new WaitForSeconds(t);
        ReactionGroomingPanelAppear();
    }


}
