﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroomingSystem : MonoBehaviour
{
    public GroomingMode groomingMode;
    public GroomingStage groomingStage;
    public GameObject groomingStation;
    public Vector3 groomObjPodiumLocalPos;
    public GameObject groomAvatar;
    public AvatarAttributes groomAvatarAttr;
    public AvatarAnimatorController groomAvatarAnim;

    public static GroomingSystem instance;
    public float groomCompletionVal;
    public float groomSpeed;
    public bool groomRunning;
    bool halfProgress;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(groomingMode != GroomingMode.Menu)
        {
            if(groomCompletionVal < 100)
            {
                if (Input.GetMouseButton(0) && groomingStage == GroomingStage.ActiveGromming)
                {
                    groomCompletionVal += Time.deltaTime * 50f * groomSpeed;

                    if (!groomRunning)
                    {
                        GroomRunningStartFuntion();
                        groomRunning = true;
                    }
                    if (groomingMode == GroomingMode.Eating && !halfProgress && groomCompletionVal > 50)
                    {
                        halfProgress = true;
                        groomAvatarAnim.Drink();
                        groomAvatarAnim.EatStop();
                        FoodTool.instance.BurgerDisAppear();
                        //FoodTool.instance.CokeSnapToHand();
                    }

                    GroomRunningContinuousFuntion();
                }
                else
                {
                    if (groomRunning)
                    {
                        GroomRunningPauseFuntion();
                        groomRunning = false;
                    }
                }
            }
            else
            {
                if (groomRunning)
                {
                    GroomRunningPauseFuntion();
                    groomRunning = false;
                    groomCompletionVal = 100;
                    halfProgress = false;
                }
                
            }
            
        }
    }
    public void GroomingAvatarIntoGroomPanel(GameObject avatar )
    {
        groomAvatar = avatar;
        groomObjPodiumLocalPos = avatar.transform.localPosition;
        avatar.transform.position = groomingStation.transform.position;
        avatar.transform.SetParent(groomingStation.transform);
        groomAvatarAttr = groomAvatar.GetComponent<AvatarAttributes>();
        groomAvatarAnim = groomAvatar.GetComponent<AvatarAnimatorController>();

    }
    public void GroomingAvatarBackToPodiumPanel()
    {
        if(groomAvatar != null)
        {
            groomAvatar.transform.SetParent(PodiumSystem.instance.modelAvatarsHolder.transform);
            groomAvatar.transform.localPosition = groomObjPodiumLocalPos;
        }
        groomAvatar = null;
        groomAvatarAttr = null;
        groomAvatarAnim = null;


    }
    public void GroomSystemCloseButtonFun()
    {
        GroomingAvatarBackToPodiumPanel();
        if (PodiumSystem.instance. podiumOpeningScenerio == PodiumOpeningScenerio.SearchAModel && PodiumSystem.instance.searchedModelIndexList.Count <= 0)
        {
            
            PodiumSystem.instance.PodiumOpenScenerioFuntion(PodiumOpeningScenerio.SearchAModel);
            
        }
        else
        {
            PodiumSystem.instance.ActivatePodium();
        }
            
        UiManagePodium.instance.UiPodiumTextValuesUpdate();
    }
    public void GroomGymButtonFun()
    {
        groomingMode = GroomingMode.Gym;
        UiManageGrooming.instance.GroomingUiPanelsUpdateAsGroomingMode();
        groomCompletionVal = 0;
    }
    public void GroomFoodButtonFun()
    {
        groomingMode = GroomingMode.Eating;
        UiManageGrooming.instance.GroomingUiPanelsUpdateAsGroomingMode();
        groomCompletionVal = 0;
    }
    public void GroomMakeupButtonFun()
    {
        groomingMode = GroomingMode.MakeUp;
        UiManageGrooming.instance.GroomingUiPanelsUpdateAsGroomingMode();
        groomCompletionVal = 0;
    }
    public void GroomOutfitButtonFun()
    {
        groomingMode = GroomingMode.Outfit;
        UiManageGrooming.instance.GroomingUiPanelsUpdateAsGroomingMode();
        groomCompletionVal = 0;
    }
    public void GroomMenuButtonFun()
    {
        groomingMode = GroomingMode.Menu;
        UiManageGrooming.instance.GroomingUiPanelsUpdateAsGroomingMode();

        if(groomAvatarAttr.AvatarFullyGroomedCheck())
        {
            GroomSystemCloseButtonFun();
        }
    }
    public void GroomingDoneFuntion()
    {
        switch (groomingMode)
        {
            case GroomingMode.Menu:
                {

                    break;
                }
            case GroomingMode.Gym:
                {
                    groomAvatarAttr.weight = Weight.Fit;
                    break;
                }
            case GroomingMode.Eating:
                {
                    groomAvatarAttr.weight = Weight.Fit;
                    break;
                }
            case GroomingMode.MakeUp:
                {
                    groomAvatarAttr.makeUpState = MakeUpState.MakeUpDone;
                    break;
                }
            case GroomingMode.Outfit:
                {
                    groomAvatarAttr.outfitState = OutfitState.OutfitUpdated;
                    break;
                }
        }
        DataSavingSystem.instance.SaveLatestModelData(PodiumSystem.instance.currentSnappedShelfIndex);
    }
    public void GroomRunningStartFuntion()
    {
        switch (groomingMode)
        {
            case GroomingMode.Gym:
                {
                    groomAvatarAnim.JoggingOn();
                    break;
                }
            case GroomingMode.Eating:
                {
                    if(groomCompletionVal < 50)
                    {
                        groomAvatarAnim.Eat();
                    }
                    else
                    {
                        groomAvatarAnim.Drink();
                    }
                    break;
                }
            case GroomingMode.MakeUp:
                {
                    
                    break;
                }
            case GroomingMode.Outfit:
                {
                   
                    break;
                }
        }
    }
    public void GroomRunningPauseFuntion()
    {
        switch (groomingMode)
        {
        
            case GroomingMode.Gym:
                {
                    groomAvatarAnim.JoggingOff();
                    break;
                }
            case GroomingMode.Eating:
                {
                    
                    groomAvatarAnim.EatStop();
                    groomAvatarAnim.DrinkStop();
                    break;
                }
            case GroomingMode.MakeUp:
                {

                    break;
                }
            case GroomingMode.Outfit:
                {

                    break;
                }
        }
    }
    public void GroomRunningContinuousFuntion()
    {
        switch (groomingMode)
        {
            case GroomingMode.Gym:
                {
                    
                    break;
                }
            case GroomingMode.Eating:
                {
                    
                    break;
                }
            case GroomingMode.MakeUp:
                {

                    break;
                }
            case GroomingMode.Outfit:
                {
                    OutfitTool.instance.OutfitPositionUpdate(groomCompletionVal/100f);
                    break;
                }
        }
    }
    public void GroomPreActivityFuntion()
    {
        halfProgress = false;
        groomSpeed = 0.5f;
        switch (groomingMode)
        {
            
            case GroomingMode.Gym:
                {
                    TreadmillTool.instance.CentreTheTool();
                    TreadmillTool.instance.avatarOnIt = groomAvatar;
                    groomAvatarAnim.TreadMilUp();
                    break;
                }
            case GroomingMode.Eating:
                {
                    FoodTool.instance.FoodAvatarOnItAssign(groomAvatar);
                    FoodTool.instance.CentreTheTool();
                    FoodTool.instance.BurgerSnapToRoot();
                    FoodTool.instance.CokeSnapToRoot();
                    groomAvatarAnim.BurgerTake();
                    
                    break;
                }
            case GroomingMode.MakeUp:
                {
                    MakeupTool.instance.MakeUpAvatarOnItAssign(groomAvatar);
                    MakeupTool.instance.CentreTheTool();
                    MakeupTool.instance.MakeUpBoxAnimReset();
                    groomAvatarAnim.Sit();
                    PodiumSystem.instance.CameraZoom();
                    break;
                }
            case GroomingMode.Outfit:
                {
                    OutfitTool.instance.OutfitAvatarOnItAssign(groomAvatar);
                    OutfitTool.instance.CentreTheTool();
                    OutfitTool.instance.OutfitVisualReset();
                    OutfitTool.instance.RightOutfitEnable(groomAvatarAttr);

                    groomSpeed = 1f;
                    break;
                }
        }
    }
    public void DoneGroomingActivityFuntion()
    {
        switch (groomingMode)
        {

            case GroomingMode.Gym:
                {
                    groomAvatarAnim.TreadMilDown();
                    groomAvatarAnim.JoggingOff();
                    TreadmillTool.instance.ActionOnDisAppear();
                    
                    break;
                }
            case GroomingMode.Eating:
                {
                    groomAvatarAnim.EatStop();
                    groomAvatarAnim.DrinkStop();
                    groomAvatarAnim.FoodDone();
                    //FoodTool.instance.CokeSnapToRoot();

                    break;
                }
            case GroomingMode.MakeUp:
                {
                    MakeupTool.instance.MakeUpDone();
                    groomAvatarAnim.StandUp();
                    PodiumSystem.instance.CameraDeZoom();
                    break;
                }
            case GroomingMode.Outfit:
                {
                    groomAvatarAnim.OutfitChanged();
                    ParticleControlSystem.instance.ParticlePlay(ParticleControlSystem.instance.OutfitChangeParticle);
                    break;
                }
        }
        
    }
    public void GroomReactionActivityFuntion()
    {
        switch (groomingMode)
        {

            case GroomingMode.Gym:
                {
                    groomAvatarAnim.Reaction();
                    groomAvatarAttr.MakeHerFit();
                    TreadmillTool.instance.RightMoveTheTool();
                    ParticleControlSystem.instance.ParticlePlay(ParticleControlSystem.instance.healthFitParticle);
                    break;
                }
            case GroomingMode.Eating:
                {
                    
                    groomAvatarAnim.Reaction();
                    groomAvatarAttr.MakeHerFit();

                    FoodTool.instance.RightMoveTheTool();
                    ParticleControlSystem.instance.ParticlePlay(ParticleControlSystem.instance.healthFitParticle);
                    break;
                }
            case GroomingMode.MakeUp:
                {
                    
                    groomAvatarAnim.Reaction();
                    MakeupTool.instance.RightMoveTheTool();
                    ParticleControlSystem.instance.ParticlePlay(ParticleControlSystem.instance.makeUpDoneParticle);
                    break;
                }
            case GroomingMode.Outfit:
                {
                    
                    groomAvatarAnim.Reaction();
                    OutfitTool.instance.RightMoveTheTool();
                    //ParticleControlSystem.instance.ParticlePlay(ParticleControlSystem.instance.OutfitChangeParticle);
                    break;
                }
        }
        
    }
    public void GroomPostActivityFuntion()
    {
        switch (groomingMode)
        {

            case GroomingMode.Gym:
                {
                    
                    break;
                }
            case GroomingMode.Eating:
                {
                    
                    break;
                }
            case GroomingMode.MakeUp:
                {
                    
                    break;
                }
            case GroomingMode.Outfit:
                {
                    
                    break;
                }
        }
    }
    public IEnumerator GroomPreActivityFuntionCoroutine(float t)
    {
        yield return new WaitForSeconds(t);
        GroomPreActivityFuntion();
    }

    


}
