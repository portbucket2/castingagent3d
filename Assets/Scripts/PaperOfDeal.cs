﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaperOfDeal : MonoBehaviour
{
    public Transform customerMangerRootPos;
    public Transform scanPos;
    public Transform intoPlayerPos;
    public bool snapping;
    public Transform targetTrans;
    public float speed;
    public float timeNeeded;

    public static PaperOfDeal instance;

    public Animator scrollAnim;
    public Material scrollMat;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        PaperInstantSnapTo(customerMangerRootPos);
    }

    // Update is called once per frame
    void Update()
    {
        if (snapping)
        {
            transform.position = Vector3.Lerp(transform.position , targetTrans.position , Time.deltaTime*speed * 5);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetTrans.rotation, Time.deltaTime * speed * 5 );
            if (Vector3.Distance( transform.position, targetTrans.position) < 0.01f)
            {
                snapping = false;
                transform.position = targetTrans.position;
                transform.rotation= targetTrans.rotation;
            }

            timeNeeded += Time.deltaTime;
        }
        
        
    }

    public void PaperInstantSnapTo(Transform trans)
    {
        transform.SetParent(trans);
        transform.position = trans.position;
        transform.rotation = trans.rotation;
        snapping = false;
    }

    public void PaperSmoothSnapTo(Transform trans, float speeed)
    {
        transform.SetParent(trans);
        speed = speeed ;
        targetTrans = trans;

        snapping = true;
        timeNeeded = 0;
    }
    public void PaperSmmothSnapTo(Transform trans)
    {

        transform.SetParent(trans);
        targetTrans = trans;

        snapping = true;
        timeNeeded = 0;
    }

    IEnumerator DelayedSmoothSnap(Transform trans, float t)
    {
        yield return new WaitForSeconds(t);
        PaperSmmothSnapTo(trans);


    }
    public void DelayedSmoothSnapTo(Transform trans, float t)
    {
        StartCoroutine(DelayedSmoothSnap(trans, t));
    }

    IEnumerator DelayedInstantSnap(Transform trans, float t)
    {
        yield return new WaitForSeconds(t);
        PaperInstantSnapTo(trans);


    }
    public void DelayedInstantSnapTo(Transform trans, float t)
    {
        StartCoroutine(DelayedInstantSnap(trans, t));
    }

    public void UnfoldScroll()
    {
        if(scrollAnim != null)
        scrollAnim.SetBool("unfold", true);

    }
    public void FoldScroll()
    {
        if (scrollAnim != null)
            scrollAnim.SetBool("unfold", false);
    }
    public void FoldScroll(float t)
    {
        Invoke("FoldScroll", t);
    }
    public void PPAssignToScroll()
    {
        if (GameStatesControl.instance.customerManager.currentCustomerAttributes.avatarVisual != null)
        {
            Sprite sprite = GameStatesControl.instance.customerManager.currentCustomerAttributes.avatarVisual.GetComponent<AvatarAttributes>().charPP;
            Texture2D tex = textureFromSprite(sprite);
            if(scrollMat != null)
            {
                scrollMat.SetTexture("Texture2D_DD83023E", tex);
            }
        }
        
    }
    public static Texture2D textureFromSprite(Sprite sprite)
    {
        if (sprite.rect.width != sprite.texture.width)
        {
            Texture2D newText = new Texture2D((int)sprite.rect.width, (int)sprite.rect.height);
            Color[] newColors = sprite.texture.GetPixels((int)sprite.textureRect.x,
                                                         (int)sprite.textureRect.y,
                                                         (int)sprite.textureRect.width,
                                                         (int)sprite.textureRect.height);
            newText.SetPixels(newColors);
            newText.Apply();
            return newText;
        }
        else
            return sprite.texture;
    }
}
