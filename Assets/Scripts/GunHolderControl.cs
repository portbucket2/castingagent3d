﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunHolderControl : MonoBehaviour
{
    public GameObject gunInHand;
    public GameObject gunInPocket;

    // Start is called before the first frame update
    void Start()
    {
        GunInHandDisAppear();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void GunInHandAppear()
    {
        gunInHand.SetActive(true);
        gunInPocket.SetActive(false);
    }
    public void GunInHandDisAppear()
    {
        gunInHand.SetActive(false);
        gunInPocket.SetActive(true);
    }
}
