﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
public class DoorAnimControl : MonoBehaviour
{
    
    public static       DoorAnimControl instance;
    private Animator    doorAnim;

    private void Awake()
    {
        instance = this;
        doorAnim = this.GetComponentInChildren<Animator>();
    }

    
    public void DoorOpen()
    {
        doorAnim.SetTrigger("openDoor");
    }

    
}
