﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PodiumSystem : MonoBehaviour
{
    public static PodiumSystem instance;
    public int modelBaseCount;
    public Transform[] cameraTransforms;
    Camera camMain;
    public GameObject podiumHolder;
    public GameObject modelBasesHolder;
    public GameObject modelAvatarsHolder;
    public PodiumOpeningScenerio podiumOpeningScenerio;
    public PawnShopShelfSliding pawnShopShelfSliding;
    public GameObject modelStandingBase;
    public int currentSnappedShelfIndex;
    public List<ModelStandingBase> modelStandingBaseScripts;
    public List<GameObject> modelAvatars;
    public List<AvatarAttributes> modelAvatarScripts;
    public List<int> searchedModelIndexList;
    bool cameraZoomed;
    
    public AnimationCurve smoothCurve;
    float fov;
    float oldFov;
    float newFov;
    // Start is called before the first frame update
    private void Awake()
    {
        instance = this;
        camMain = Camera.main;
        GameObject CameraGameRef = new GameObject("CameraGameRef");
        CameraGameRef.transform.position = camMain.transform.position;
        CameraGameRef.transform.rotation = camMain.transform.rotation;

        cameraTransforms[0] = CameraGameRef.transform;
        CameraGameRef.transform.SetParent(this.transform);
    }
    void Start()
    {
        InitiatePodium();
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.M))
        //{
        //    AddAStandingBaseToPodium();
        //}
        //if (Input.GetKeyDown(KeyCode.N))
        //{
        //    AddAModelToPodium(GameStatesControl.instance.customerManager.currentCustomerAttributes);
        //}
        //if (Input.GetKeyDown(KeyCode.B))
        //{
        //    RemoveAModelFromPodium();
        //}

        CameraZoomControl();
    }

    public void SwitchCameraTo(CameraMode camMode)
    {
        switch (camMode)
        {
            case CameraMode.GameCamera:
                {
                    camMain.transform.position = cameraTransforms[0].position;
                    camMain.transform.rotation = cameraTransforms[0].rotation;
                    break;
                }
            case CameraMode.PodiumCamera:
                {
                    camMain.transform.position = cameraTransforms[1].position;
                    camMain.transform.rotation = cameraTransforms[1].rotation;
                    break;
                }
            case CameraMode.GroomCamera:
                {
                    camMain.transform.position = cameraTransforms[2].position;
                    camMain.transform.rotation = cameraTransforms[2].rotation;
                    break;
                }
        }
        GameStatesControl.instance.uiManagerMain.GetRightUiPanelMode(camMode);
    }

    public void DeactivatePodium()
    {
        SwitchCameraTo(CameraMode.GameCamera);
        podiumHolder.SetActive(false);

        PodiumCloseScenerioFuntion();
        SetPodiumOpenScenerioTo(PodiumOpeningScenerio.IdleOpen);
    }
    public void ActivatePodium()
    {
        if(podiumOpeningScenerio != PodiumOpeningScenerio.SearchAModel)
        {
            pawnShopShelfSliding.shelfTouchInputs.TouchUnBlockInPodium();
            
        }
        
        podiumHolder.SetActive(true);
        
        SwitchCameraTo(CameraMode.PodiumCamera);

        

    }
    public void ActivatePodiumIdle()
    {
        PodiumOpenScenerioFuntion(PodiumOpeningScenerio.IdleOpen);
    }
    public void GroomSystemAppear()
    {
        SwitchCameraTo(CameraMode.GroomCamera);
        podiumHolder.SetActive(false);
        if((modelAvatars.Count -1) >= currentSnappedShelfIndex)
        {
            GroomingSystem.instance.GroomingAvatarIntoGroomPanel(modelAvatars[currentSnappedShelfIndex]);
            UiManageGrooming.instance.GroomingUiPanelsUpdateAsGroomingMode();
        }
    }
    public void SetPodiumOpenScenerioTo(PodiumOpeningScenerio podiumOpen)
    {
        podiumOpeningScenerio = podiumOpen;
    }
    public void PodiumOpenScenerioFuntion(PodiumOpeningScenerio podiumScenerio)
    {
        SetPodiumOpenScenerioTo( podiumScenerio);
        switch (podiumOpeningScenerio)
        {
            case PodiumOpeningScenerio.IdleOpen:
                {
                    pawnShopShelfSliding.shelfTouchInputs.TouchUnBlockInPodium();
                    ActivatePodium();
                    break;
                }
            case PodiumOpeningScenerio.ModelAdded:
                {
                    pawnShopShelfSliding.shelfTouchInputs.TouchUnBlockInPodium();
                    ActivatePodium();
                    AddAModelToPodium(GameStatesControl.instance.customerManager.currentCustomerAttributes.avatarVisual);
                    DataSavingSystem.instance.SaveLatestModelData(PodiumSystem.instance.modelAvatars.Count - 1);
                    break;
                }
            case PodiumOpeningScenerio.SearchAModel:
                {
                    
                    ActivatePodium();
                    SearcehedModelListCreation();
                    pawnShopShelfSliding.shelfTouchInputs.TouchBlockInPodium();
                    break;
                }
        }
        UiManagePodium.instance.UiPodiumTextValuesUpdate();
    }
    public void PodiumCloseScenerioFuntion()
    {
        
        switch (podiumOpeningScenerio)
        {
            case PodiumOpeningScenerio.IdleOpen:
                {
                    break;
                }
            case PodiumOpeningScenerio.ModelAdded:
                {
                    GameStatesControl.instance. NextLevel();
                    break;
                }
            case PodiumOpeningScenerio.SearchAModel:
                {
                    break;
                }
        }
    }
    public void AddAStandingBaseToPodium()
    {
        pawnShopShelfSliding.shelfCount += 1;
        GameObject go = Instantiate(modelStandingBase, modelBasesHolder. transform.position,modelBasesHolder. transform.rotation);
        go.transform.localRotation = Quaternion.Euler( new Vector3(0, 180, 0));
        go.transform.SetParent(modelBasesHolder.transform);
        go.transform.localPosition = new Vector3((pawnShopShelfSliding.shelfCount - 1) * pawnShopShelfSliding.shelfWidth, 0, 0);
        modelStandingBaseScripts.Add(go.GetComponent<ModelStandingBase>());
        go.GetComponent<ModelStandingBase>().modelBaseId = modelStandingBaseScripts.Count - 1;
        go.GetComponent<ModelStandingBase>().modelStandingBaseEmptyness = ModelStandingBaseEmptyness.Empty;
    }
    public void InitiatePodium()
    {
        for (int i = 0; i < modelBaseCount; i++)
        {
            AddAStandingBaseToPodium();
        }
        DataSavingSystem.instance.SavedDataRetrieveForPodium();
        DeactivatePodium();
    }
    public void AddAModelToPodium(GameObject avatarGo)
    {
        if(modelAvatarScripts.Count >= modelStandingBaseScripts.Count)
        {
            AddAStandingBaseToPodium();
            //return;
        }
        DollarForDeal.instance.PaperInstantSnapTo(DollarForDeal.instance.customerMangerRootPos);
        PaperOfDeal.instance.PaperInstantSnapTo(PaperOfDeal.instance.customerMangerRootPos);
        GameObject go = Instantiate(avatarGo, modelStandingBaseScripts[(modelAvatarScripts.Count )].transform.position, modelStandingBaseScripts[(modelAvatarScripts.Count )].transform.rotation);
        modelStandingBaseScripts[(modelAvatarScripts.Count)].modelStandingBaseEmptyness = ModelStandingBaseEmptyness.HasModel;
        go.transform.SetParent(modelAvatarsHolder.transform);
        modelAvatars.Add(go);
        modelAvatarScripts.Add(go.GetComponent<AvatarAttributes>());
        
        //modelStandingBaseScripts[modelAvatarScripts.Count - 1].avatarAttributes = modelAvatarScripts[modelAvatarScripts.Count - 1].gameObject.GetComponent<AvatarAttributes>();


        pawnShopShelfSliding.GoSnapToShelf(modelAvatarScripts.Count - 1);

        
    }
    //public void AddAModelToPodium(CustomerAttributes cusAttr)
    //{
    //    if (modelAvatarScripts.Count >= modelStandingBaseScripts.Count)
    //    {
    //        AddAStandingBaseToPodium();
    //        //return;
    //    }
    //    DollarForDeal.instance.PaperInstantSnapTo(DollarForDeal.instance.customerMangerRootPos);
    //    PaperOfDeal.instance.PaperInstantSnapTo(PaperOfDeal.instance.customerMangerRootPos);
    //    GameObject go = Instantiate(cusAttr.avatarVisual, modelStandingBaseScripts[(modelAvatarScripts.Count)].transform.position, modelStandingBaseScripts[(modelAvatarScripts.Count)].transform.rotation);
    //    modelStandingBaseScripts[(modelAvatarScripts.Count)].modelStandingBaseEmptyness = ModelStandingBaseEmptyness.HasModel;
    //    go.transform.SetParent(modelAvatarsHolder.transform);
    //    modelAvatars.Add(go);
    //    modelAvatarScripts.Add(go.GetComponent<AvatarAttributes>());
    //
    //    //modelStandingBaseScripts[modelAvatarScripts.Count - 1].avatarAttributes = modelAvatarScripts[modelAvatarScripts.Count - 1].gameObject.GetComponent<AvatarAttributes>();
    //
    //
    //    pawnShopShelfSliding.GoSnapToShelf(modelAvatarScripts.Count - 1);
    //
    //    DataSavingSystem.instance.SaveModelData();
    //}
    public void RemoveAModelFromPodium()
    {
        GameObject go = modelAvatars[currentSnappedShelfIndex];
        modelAvatars.RemoveAt(currentSnappedShelfIndex);
        modelAvatarScripts.RemoveAt(currentSnappedShelfIndex);

        Destroy(go);
        for (int i = 0; i < modelAvatars.Count; i++)
        {
            modelAvatars[i].transform.position = modelStandingBaseScripts[i].gameObject.transform.position;
            modelAvatars[i].transform.rotation = modelStandingBaseScripts[i].gameObject.transform.rotation;
        }

        DataSavingSystem.instance.SaveModelData();
    }
    public void SellModelToDirector()
    {
        RemoveAModelFromPodium();
        GameStatesControl.instance.gameManagerMain.ChangeGameStateTo(GameState.DealResult);
        GameStatesControl.instance.customerManager. currentCustomerAttributes.ReactionBubbleAppear(CustomerReaction.Happy);
        GameStatesControl.instance.customerManager.currentCustomerAttributes.avatarVisual.GetComponent<AvatarAnimatorController>().Accepted();
        DeactivatePodium();
    }
    public void QuickSellModel()
    {
        AvatarAttributes avatarAttributes = PodiumSystem.instance.modelAvatarScripts[PodiumSystem.instance.currentSnappedShelfIndex];
        AccountManager.instance.BalanceAddBy(avatarAttributes.estimatedWorth);
        RemoveAModelFromPodium();
        //GameStatesControl.instance.gameManagerMain.ChangeGameStateTo(GameState.DealResult);
        //GameStatesControl.instance.customerManager.currentCustomerAttributes.ReactionBubbleAppear(CustomerReaction.Happy);
        //GameStatesControl.instance.customerManager.currentCustomerAttributes.avatarVisual.GetComponent<AvatarAnimatorController>().Accepted();
        UiManagePodium.instance.QuickSellPanelDisAppear();
        DeactivatePodium();

        ParticleControlSystem.instance.ParticlePlay(ParticleControlSystem.instance.confettiParticle);

    }
    public void SearcehedModelListCreation()
    {
        searchedModelIndexList.Clear();
        CustomerAttributes cusAttr = GameStatesControl.instance.customerManager.currentCustomerAttributes;
        for (int i = 0; i < modelAvatarScripts.Count; i++)
        {
            bool added = false;
            if (cusAttr.heightRequirement != Height.Average)
            {
                if(modelAvatarScripts[i].height == cusAttr.heightRequirement)
                {
                    if (!added)
                    {
                        searchedModelIndexList.Add(i);
                        added = true;
                    }
                }
            }
            if (cusAttr.weightRequirement != Weight.Average)
            {
                if (modelAvatarScripts[i].weight == cusAttr.weightRequirement)
                {
                    if (!added)
                    {
                        searchedModelIndexList.Add(i);
                        added = true;
                    }
                }
            }
            if (cusAttr.skinToneRequirement != SkinTone.Average)
            {
                if (modelAvatarScripts[i].skinTone == cusAttr.skinToneRequirement)
                {
                    if (!added)
                    {
                        searchedModelIndexList.Add(i);
                        added = true;
                    }
                }
            }
        }
        if(searchedModelIndexList.Count > 0)
        {
            pawnShopShelfSliding.GoSnapToShelf(searchedModelIndexList[0]);
            UiManagePodium.instance.buttonSell.interactable = true;
        }
        else
        {
            UiManagePodium.instance.buttonSell.interactable = false;
        }
        

        
    }
    public void CameraZoomControl()
    {
        if (cameraZoomed)
        {
            fov += Time.deltaTime ;
            if (fov >= 1)
            {
                cameraZoomed = false;
                //fov = 0;
            }
            

            Camera.main.fieldOfView = Mathf.Lerp(oldFov, newFov, smoothCurve.Evaluate(fov));
        }
        
    }
    public void CameraZoom()
    {
        cameraZoomed = true;
        oldFov = Camera.main.fieldOfView;
        newFov = 17;
        fov = 0;
    }
    public void CameraDeZoom()
    {
        cameraZoomed = true;
        oldFov = Camera.main.fieldOfView;
        newFov = 90;
        fov = 0;
    }
    
}
