﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvatarAttributes : MonoBehaviour
{
    public CustomerType customerType;
    public Transform paperHolder;
    public Transform dollarHolder;
    public Transform cokeHolder;
    public Transform makeUpboxHolder;
    public CustomerAttributes customerAttributes;
    public int avatarVisualId;
    [Header("ModelInfo")]
    public string title;
    public Height   height;
    public Weight   weight;
    public SkinTone skinTone;
    public MakeUpState makeUpState;
    public OutfitState outfitState;
    public int dealValue;
    public int estimatedWorth;
    public Sprite charPP;
    [Header("MeshInfo")]
    public SkinnedMeshRenderer[] skinnedMesh;
    public float healthyVal;
    public float skinnyVal;
    float targetHealthyVal;
    float targetSkinnyVal;
    public bool HealthTransition;
    public float HealthBlendMultiplier;
    public float transitionSpeed;
    public int avatarOutfitId;
    public bool fullyGroomed;
    
    // Start is called before the first frame update
    void Start()
    {
        
        
    }

    // Update is called once per frame
    void Update()
    {
        if (HealthTransition)
        {
            healthyVal = Mathf.MoveTowards(healthyVal, targetHealthyVal, Time.deltaTime * 80f * transitionSpeed* HealthBlendMultiplier);
            skinnyVal = Mathf.MoveTowards(skinnyVal, targetSkinnyVal, Time.deltaTime * 80f * transitionSpeed* HealthBlendMultiplier);
            ApplyHealthToMesh();
            if(healthyVal == targetHealthyVal && skinnyVal == targetSkinnyVal)
            {
                HealthTransition = false;
            }
        }
    }
    public void AvatarInitialTasks()
    {
        PaperAndDollarPositioning(customerAttributes.customerType);
        GetAvatarDataFrom(customerAttributes);
        HealthTypeSettingAtInitialization();
        GetComponent<AvatarShadersControl>().ShadersUpdatingAsCurrentCondition();
        PaperOfDeal.instance.PPAssignToScroll();
    }
    public void PaperAndDollarPositioning(CustomerType cusTyp)
    {
        switch (cusTyp)
        {
            case CustomerType.Model:
                {
                    PaperOfDeal.instance.PaperInstantSnapTo(paperHolder);
                    DollarForDeal.instance.DollarAppearScaled();
                    DollarForDeal.instance.PaperInstantSnapTo(DollarForDeal.instance.intoPlayerPos);
                    break;
                }
            case CustomerType.Director:
                {
                    PaperOfDeal.instance.PaperInstantSnapTo(PaperOfDeal.instance.customerMangerRootPos);
                    DollarForDeal.instance.PaperInstantSnapTo(dollarHolder);
                    DollarForDeal.instance.DollarDisAppearScaled();
                    break;
                }
            case CustomerType.Robber:
                {
                    PaperOfDeal.instance.PaperInstantSnapTo(PaperOfDeal.instance.customerMangerRootPos);
                    DollarForDeal.instance.PaperInstantSnapTo(DollarForDeal.instance.intoPlayerPos);
                    DollarForDeal.instance.DollarAppearScaled();
                    break;
                }
        }
    }

    public void GetAvatarDataFrom(CustomerAttributes cusAttr)
    {
        title           = cusAttr.title;
        height          = cusAttr. height;
        weight          = cusAttr. weight;
        skinTone        = cusAttr.skinTone;
        makeUpState     = cusAttr.makeUpState;
        outfitState     = cusAttr.outfitState;
        dealValue       = cusAttr.dealValue                ;
        estimatedWorth  = cusAttr.estimatedWorth;
        avatarVisualId = cusAttr.avatarId;
    }
    public void HealthTypeSettingAtInitialization() { 
        switch (weight)
        {
            case (Weight.Average):
                {
                    skinnyVal = 0;
                    healthyVal = 0;
                    break;
                }
            case (Weight.Fit):
                {
                    skinnyVal = 0;
                    healthyVal = 0;
                    break;
                }
            case (Weight.Skinny):
                {
                    skinnyVal = 100 * HealthBlendMultiplier ;
                    healthyVal = 0;
                    break;
                }
            case (Weight.Healthy):
                {
                    skinnyVal = 0;
                    healthyVal = 100 * HealthBlendMultiplier;
                    break;
                }

        }
        ApplyHealthToMesh();
    }
    void ApplyHealthToMesh()
    {
        if (skinnedMesh != null)
        {
            for (int i = 0; i < skinnedMesh.Length; i++)
            {
                skinnedMesh[i].SetBlendShapeWeight(0, skinnyVal);
                skinnedMesh[i].SetBlendShapeWeight(1, healthyVal);
            }
        }
    }
    public void MakeHerFit()
    {
        targetHealthyVal = 0;
        targetSkinnyVal = 0;
        HealthTransition = true;
        weight = Weight.Fit;
    }

    public bool AvatarFullyGroomedCheck()
    {
        bool result = false;
        if(weight == Weight.Fit && makeUpState == MakeUpState.MakeUpDone && outfitState == OutfitState.OutfitUpdated)
        {
            fullyGroomed = true;
            result = true;
        }
        else
        {
            fullyGroomed = false;
            result = false;
        }
        return result;
    }

    
}
