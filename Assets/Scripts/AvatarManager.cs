﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvatarManager : MonoBehaviour
{
    public static AvatarManager instance;
    public GameObject[] avatarsInside;
    
    
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GetAvatar(int index , Transform trans)
    {
        GameObject go = avatarsInside[index];
        go.transform.position = trans.position;
        go.transform.rotation = trans.rotation;
        go.transform.SetParent(trans);
        trans.gameObject.GetComponent<CustomerAttributes>().avatarVisual = go;
        go.GetComponent<AvatarAttributes>().customerAttributes = trans.gameObject.GetComponent<CustomerAttributes>();

        go.GetComponent<AvatarAttributes>().AvatarInitialTasks();
    }

    public void BackAvatarToParent(int index)
    {
        avatarsInside[index].transform.position = transform.position;
        avatarsInside[index].transform.rotation = transform.rotation;
        avatarsInside[index].transform.SetParent(transform);
        avatarsInside[index].GetComponent<AvatarAttributes>().customerAttributes = null;
    }
}
