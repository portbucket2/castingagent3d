﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AccountManager : MonoBehaviour
{
    public static AccountManager instance;
    public int balanceCurrent;
    public float balanceDisplayed;
    public bool balanceNotUpdated;
    public Text textBalance;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        balanceNotUpdated = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (balanceNotUpdated)
        {
            
            balanceDisplayed =(Mathf.Lerp((float)balanceDisplayed,(float) balanceCurrent, Time.deltaTime * 5));
            if(Mathf.Abs((float)balanceDisplayed- (float)balanceCurrent) < 1)
            {
                balanceDisplayed = balanceCurrent;
                balanceNotUpdated = false;
            }
            textBalance.text = "" + (int)balanceDisplayed;
        }
    }
    public void BalanceAddBy(int m)
    {
        balanceCurrent += m;
        balanceNotUpdated = true;

        DataSavingSystem.instance.BalanceCurrentSet(balanceCurrent);
    }
    public void BalanceReduceBy(int m)
    {
        balanceCurrent -= m;
        balanceNotUpdated = true;
        if (balanceCurrent < 0)
        {
            balanceCurrent = 0;
            //ShittyFix
            balanceCurrent = 1000;
        }

        DataSavingSystem.instance.BalanceCurrentSet(balanceCurrent);
    }
}
