﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CustomerAttributes : MonoBehaviour
{
    [Header("AsAModel")]
    public string title;
    public CustomerType customerType;
    public ModelType modelType;
    public Height height;
    public Weight weight;
    public SkinTone skinTone;
    public MakeUpState makeUpState;
    public OutfitState outfitState;
    public int dealValue;
    public int estimatedWorth;

    [Header("AsADirector")]
    public Height heightRequirement;
    public Weight weightRequirement;
    public SkinTone skinToneRequirement;
    public int directorOfferPrice;
    [Header("AsAnAvatar")]
    public int avatarId;
    public GameObject avatarVisual;
    [Header("AsARobber")]
    public int robbingAmount;

    [Header("Movement")]
    public bool moving;
    public Transform targetPos;
    public Transform acceptedTargetPos;
    public Transform rejectedTargetPos;
    public float moveSpeed;
    [Header("UI")]
    public GameObject welcomePanel;
    public GameObject sellerPanel;
    public GameObject buyerPanel;
    public GameObject RobberPanel;
    public GameObject ReactionPanel;
    public Text reactionText;
    public Text titleText;
    public Text dealValueText;
    public Text requirementText;
    public Text robbingAmountText;



    // Start is called before the first frame update
    void Start()
    {
        CloseSpeechBubble();
    }

    // Update is called once per frame
    void Update()
    {
        if(moving && targetPos)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetPos.position, Time.deltaTime * moveSpeed);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, targetPos.rotation, Time.deltaTime * moveSpeed * 40f);
        }
        //if(targetPos != null && transform.position == targetPos.position && moving)
        //{
        //    moving = false;
        //}

    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "pathPoint")
        {
            other.GetComponent<PathPoint>().PathPointFunction(this);
        }
        
    }
    public void AcceptedPathFollow()
    {
        if (acceptedTargetPos != null)
        {
            targetPos = acceptedTargetPos;
        }
        moving = true;
    }
    public void RejectedPathFollow()
    {
        if(rejectedTargetPos != null)
        {
            targetPos = rejectedTargetPos;
        }
        moving = true;
    }

    public void SpeechBubbleAppear()
    {
        CloseSpeechBubble();
        switch (customerType)
        {
            case CustomerType.Model:
                {
                    dealValueText.text = "$ "+dealValue;
                    sellerPanel.SetActive(true);
                    break;
                }
            case CustomerType.Director:
                {
                    SetRequirementText();
                    buyerPanel.SetActive(true);
                    break;
                }
            case CustomerType.Robber:
                {
                    robbingAmountText.text ="$ "+ robbingAmount;
                    RobberPanel.SetActive(true);
                    break;
                }
        }
    }
    public void SetRequirementText()
    {
        string requirement = "";
        List<string> req = new List<string>();
        if (heightRequirement != Height.Average)
        {
            string req1 =""+ heightRequirement;
            req.Add(req1);
        }
        if(weightRequirement != Weight.Average)
        {
            string req1 =""+ weightRequirement;
            req.Add(req1);
        }
        if(skinToneRequirement != SkinTone.Average)
        {
            string req1 =""+ skinToneRequirement;
            req.Add(req1);
        }
        switch (req.Count)
        {
            case 0:
                {
                    requirement = "Average";
                    break;
                }
            case 1:
                {
                    requirement = req[0];
                    break;
                }
            case 2:
                {
                    requirement = req[0] + " & " + req[1];
                    break;
                }
            case 3:
                {
                    requirement = req[0] + ", " + req[1] + " & " + req[2];
                    break;
                }
        }

        requirementText.text = requirement;
    }
    public void ReactionBubbleAppear(CustomerReaction cusReact)
    {
        CloseSpeechBubble();
        ReactionPanel.SetActive(true);
        switch (cusReact)
        {
            case CustomerReaction.VeryHappy:
                {
                    reactionText.text = "Payday baby!";
                    break;
                }
            case CustomerReaction.Happy:
                {
                    reactionText.text = "Thank You";
                    break;
                }
            case CustomerReaction.Ok:
                {
                    reactionText.text = "Nice";
                    break;
                }
            case CustomerReaction.Sad:
                {
                    reactionText.text = "Not Good";
                    break;
                }
            case CustomerReaction.VerySad:
                {
                    reactionText.text = "F*ck You Bitch";
                    break;
                }
        }
    }
    public void WelcomeBubbleAppear()
    {
        titleText.text = title;
        CloseSpeechBubble();
        welcomePanel.SetActive(true);
    }
    public void CloseSpeechBubble()
    {
        welcomePanel.SetActive(false);
        sellerPanel.SetActive(false);
        buyerPanel.SetActive(false);
        RobberPanel.SetActive(false);
        ReactionPanel.SetActive(false);
    }
}
