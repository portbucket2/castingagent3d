﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomerManager : GameManagerMain
{
    public GameObject initialTargetPathPoint;
    public CustomerAttributes[] customers;
    public int[] customerAppearOrder;
    public int currentCustomerIndex;
    public GameObject currentCustomer;
    public CustomerAttributes currentCustomerAttributes;
    

    // Start is called before the first frame update
    void Start()
    {
        RegisterActions();
        OnStartLevel += SpawnTheCustomer;
       OnSayOfferCustomer += CustomerSpeechBubbleAppear;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            currentCustomerAttributes.AcceptedPathFollow();
            
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            currentCustomerAttributes.RejectedPathFollow();
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            SpawnTheCustomer();
            //gameStatesControl = base.gameStatesControl;
            
        }
    }
    
    public void SpawnTheCustomer()
    {
        currentCustomerIndex = customerAppearOrder[GameStatesControl.instance.levelIndex];
        currentCustomer = Instantiate(customers[currentCustomerIndex].gameObject, transform.position, transform.rotation);
        currentCustomerAttributes = currentCustomer.GetComponent<CustomerAttributes>();
        currentCustomerAttributes.targetPos = initialTargetPathPoint.transform;
        currentCustomerAttributes.moving = true;

        AvatarManager.instance.GetAvatar(currentCustomerAttributes.avatarId, currentCustomer.transform);

        currentCustomerAttributes.avatarVisual.GetComponent<AvatarAnimatorController>().Walk();
    }
    public void CustomerSpeechBubbleAppear()
    {
        currentCustomer.GetComponent<CustomerAttributes>().SpeechBubbleAppear();
    }

    public void DealResultOnCustomer(DealingResultType dealResult)
    {
        
        switch (dealResult)
        {
            case DealingResultType.Accepted:
                {
                    if(currentCustomerAttributes.customerType == CustomerType.Model)
                    {
                        GameStatesControl.instance.SetLevelEndType(LevelEndType.ModelAdded);

                        PaperOfDeal.instance.DelayedSmoothSnapTo(PaperOfDeal.instance.intoPlayerPos, 1f);
                        DollarForDeal.instance.DelayedSmoothSnapTo(DollarForDeal.instance.scanPos, 1.2f);
                        PaperOfDeal.instance.FoldScroll();

                        GameStatesControl.instance.gameManagerMain.ChangeGameStateTo(GameState.DealResult);
                        currentCustomerAttributes.ReactionBubbleAppear(CustomerReaction.Happy);

                        AccountManager.instance.BalanceReduceBy(currentCustomerAttributes.dealValue);

                        GameStatesControl.instance.customerManager.currentCustomerAttributes.avatarVisual.GetComponent<AvatarAnimatorController>().Accepted();
                    }
                    else if (currentCustomerAttributes.customerType == CustomerType.Director)
                    {

                        PodiumSystem.instance.PodiumOpenScenerioFuntion(PodiumOpeningScenerio.SearchAModel);
                        
                    }
                    //currentCustomerAttributes.AcceptedPathFollow();

                    
                    break;
                }

            case DealingResultType.Rejected:
                {
                    //currentCustomerAttributes.RejectedPathFollow();
                    GameStatesControl.instance.gameManagerMain.ChangeGameStateTo(GameState.DealResult);
                    currentCustomerAttributes.ReactionBubbleAppear(CustomerReaction.Sad);

                    GameStatesControl.instance.customerManager.currentCustomerAttributes.avatarVisual.GetComponent<AvatarAnimatorController>().Rejected();
                    break;
                }
            case DealingResultType.PoliceCalled:
                {
                    //currentCustomerAttributes.RejectedPathFollow();
                    break;
                }
            case DealingResultType.Slapped:
                {
                    GameStatesControl.instance.gameManagerMain.ChangeGameStateTo(GameState.DealResult);
                    currentCustomerAttributes.ReactionBubbleAppear(CustomerReaction.Sad);

                    GameStatesControl.instance.customerManager.currentCustomerAttributes.avatarVisual.GetComponent<AvatarAnimatorController>().Rejected();
                    ScannerControl.instance.SneakerThrow();
                    //currentCustomerAttributes.RejectedPathFollow();
                    break;
                }
            case DealingResultType.Robbed:
                {
                    GameStatesControl.instance.gameManagerMain.ChangeGameStateTo(GameState.DealResult);
                    currentCustomerAttributes.ReactionBubbleAppear(CustomerReaction.VeryHappy);

                    GameStatesControl.instance.customerManager.currentCustomerAttributes.avatarVisual.GetComponent<AvatarAnimatorController>().Accepted();
                    
                    DollarForDeal.instance.DelayedSmoothSnapTo(DollarForDeal.instance.scanPos, 0);
                    AccountManager.instance.BalanceReduceBy(GameStatesControl.instance.customerManager.currentCustomerAttributes.robbingAmount);
                    break;
                }
        }

        
    }

    public void DealClosingOnTableOnCustomer()
    {
        DealingResultType dealResult = GameStatesControl.instance.currentDealResult;
        if (currentCustomerAttributes.customerType != CustomerType.Model)
        {
            GameStatesControl.instance.LevelIndexIncrease();
        }
        switch (dealResult)
        {
            case DealingResultType.Accepted:
                {
                    if (currentCustomerAttributes.customerType == CustomerType.Model)
                    {

                        Transform t = currentCustomerAttributes.avatarVisual.GetComponent<AvatarAttributes>().dollarHolder;
                        DollarForDeal.instance.DelayedSmoothSnapTo(t, 0.5f);

                        GameStatesControl.instance.customerManager.currentCustomerAttributes.avatarVisual.GetComponent<AvatarAnimatorController>().PaperOnTable();
                    }
                    else if(currentCustomerAttributes.customerType == CustomerType.Director)
                    {
                        DollarForDeal.instance.DollarAppearScaled();
                        DollarForDeal.instance.DelayedSmoothSnapTo(DollarForDeal.instance.scanPos, 0.5f);
                        AccountManager.instance.BalanceAddBy(currentCustomerAttributes.directorOfferPrice);

                        GameStatesControl.instance.customerManager.currentCustomerAttributes.avatarVisual.GetComponent<AvatarAnimatorController>().PaperOnTable();
                    }
                    //currentCustomerAttributes.AcceptedPathFollow();
                    break;
                }

            case DealingResultType.Rejected:
                {
                    if (currentCustomerAttributes.customerType == CustomerType.Model)
                    {

                        Transform t = currentCustomerAttributes.avatarVisual.GetComponent<AvatarAttributes>().paperHolder;
                        PaperOfDeal.instance.DelayedSmoothSnapTo(t, 0.5f);
                        PaperOfDeal.instance.FoldScroll(.2f);


                        GameStatesControl.instance.customerManager.currentCustomerAttributes.avatarVisual.GetComponent<AvatarAnimatorController>().PaperOnTable();
                    }

                    //currentCustomerAttributes.RejectedPathFollow();
                    break;
                }
            case DealingResultType.PoliceCalled:
                {
                    //currentCustomerAttributes.RejectedPathFollow();
                    break;
                }
            case DealingResultType.Slapped:
                {
                    //currentCustomerAttributes.RejectedPathFollow();
                    break;
                }
            case DealingResultType.Robbed:
                {
                    GameStatesControl.instance.customerManager.currentCustomerAttributes.avatarVisual.GetComponent<AvatarAnimatorController>().PaperOnTable();
                    Transform t = currentCustomerAttributes.avatarVisual.GetComponent<AvatarAttributes>().dollarHolder;
                    DollarForDeal.instance.DelayedSmoothSnapTo(t, 0.5f);
                    //currentCustomerAttributes.AcceptedPathFollow();
                    break;
                }
        }
        currentCustomerAttributes.CloseSpeechBubble();
    }
    public void DealEndOnCustomer()
    {
        DealingResultType dealResult = GameStatesControl.instance.currentDealResult;
        
        switch (dealResult)
        {
            case DealingResultType.Accepted:
                {
                    if (currentCustomerAttributes.customerType == CustomerType.Model)
                    {
                        
                    }
                    else if (currentCustomerAttributes.customerType == CustomerType.Director)
                    {

                        DollarForDeal.instance.DelayedSmoothSnapTo(DollarForDeal.instance.intoPlayerPos, 0.5f);

                    }
                    currentCustomerAttributes.AcceptedPathFollow();
                    GameStatesControl.instance.customerManager.currentCustomerAttributes.avatarVisual.GetComponent<AvatarAnimatorController>().Walk();
                    break;
                }

            case DealingResultType.Rejected:
                {
                    currentCustomerAttributes.RejectedPathFollow();
                    if (currentCustomerAttributes.customerType == CustomerType.Model)
                    {
                        GameStatesControl.instance.customerManager.currentCustomerAttributes.avatarVisual.GetComponent<AvatarAnimatorController>().Walk();
                    }
                    else if (currentCustomerAttributes.customerType == CustomerType.Director)
                    {

                        GameStatesControl.instance.customerManager.currentCustomerAttributes.avatarVisual.GetComponent<AvatarAnimatorController>().WalkSad();

                    }
                    

                    break;
                }
            case DealingResultType.PoliceCalled:
                {
                    currentCustomerAttributes.RejectedPathFollow();
                    break;
                }
            case DealingResultType.Slapped:
                {
                    currentCustomerAttributes.RejectedPathFollow();
                    if (currentCustomerAttributes.customerType == CustomerType.Robber)
                    {

                        GameStatesControl.instance.customerManager.currentCustomerAttributes.avatarVisual.GetComponent<AvatarAnimatorController>().WalkSad();

                    }
                    //GameStatesControl.instance.customerManager.currentCustomerAttributes.avatarVisual.GetComponent<AvatarAnimatorController>().WalkSad();
                    break;
                }
            case DealingResultType.Robbed:
                {
                    currentCustomerAttributes.AcceptedPathFollow();
                    GameStatesControl.instance.customerManager.currentCustomerAttributes.avatarVisual.GetComponent<AvatarAnimatorController>().Walk();
                    break;
                }
        }
        
    }
    public void WelcomeBubbleAppearFunction()
    {
        currentCustomerAttributes.WelcomeBubbleAppear();
    }
    public void LevelEndFunctionLocal()
    {
        if (currentCustomerAttributes.customerType == CustomerType.Model)
        {
            GameStatesControl.instance.LevelIndexIncrease();
        }
    }
    public void ReadyForNextLevelFunctionLocal()
    {
        //DollarForDeal.instance.PaperInstantSnapTo(DollarForDeal.instance.customerMangerRootPos);
        AvatarManager.instance.BackAvatarToParent(currentCustomerAttributes.avatarId);
        GameObject go = currentCustomer;
        currentCustomer = null;
        currentCustomerAttributes = null;
        Destroy(go);
        Debug.Log("Destroyed");
        
    }

    

    public void RegisterActions()
    {
        //OnLevelEnd += LevelEndFunction;
        //OnLevelEnd += GameStatesControl.instance.LevelEndFuntion;
        
        OnCustomerOnDesk += WelcomeBubbleAppearFunction;
        OnDealClosingOnTable += DealClosingOnTableOnCustomer;
        OnDealEnd += DealEndOnCustomer;
        OnReadyForNextLevel += ReadyForNextLevelFunctionLocal;
        OnReadyForNextLevel += GameStatesControl.instance.ReadyForNextLevelFuntion;

        OnLevelEnd += GameStatesControl.instance.LevelEndFuntionOfGameStates;
        OnLevelEnd += LevelEndFunctionLocal;
    }
}
