﻿

public enum GameState
{
    Idle,
    StartLevel,
    CustomerOnDoor,
    CustomerOnDesk,
    PaperOnTable,
    ScanningPaper,
    SayOfferCustomer,
    DealStart,
    DealResult,
    DealClosingOntable,
    DealEnd,
    LevelEnd,
    ReadyForNextLevel
}

public enum CustomerType
{
    Model,
    Director,
    Robber

}
public enum ModelType
{
    Ordinary,
    Criminal
}
public enum Height
{
    Average,
    Tall,
    Short
}
public enum Weight
{
    Average,
    Healthy,
    Fit,
    Skinny
}
public enum SkinTone
{
    Average,
    Fair,
    Tan,
    Dark
}
public enum PathPointType
{
    Ordinary,
    OpenDoorSensor,
    AtDesk,
    EndPoint
}
public enum CustomerReaction
{
    Happy,
    Sad,
    Ok,
    VeryHappy,
    VerySad
}
//public enum UiPanelMainType
//{
//    Start,
//    Dealing,
//    DealDone
//}
public enum DealingResultType
{
    NoDealingYet,
    Accepted,
    Rejected,
    PoliceCalled,
    Slapped,
    Robbed

}
public enum LevelEndType
{
    NoModelAdded,
    ModelAdded
    
}
public enum CameraMode
{
    GameCamera,
    PodiumCamera,
    GroomCamera
}

public enum PodiumOpeningScenerio
{
    IdleOpen,
    ModelAdded,
    SearchAModel
}
public enum PodiumUiMode
{
   
    InfoPanelMode,
    SearchMode
}
public enum ModelStandingBaseEmptyness
{
    Empty,
    HasModel
}
public enum GroomingMode
{
    Menu,
    Gym,
    Eating,
    MakeUp,
    Outfit
}
 public enum MakeUpState
{
    Ordinary,
    MakeUpDone
}
public enum OutfitState
{
    Ordinary,
    OutfitUpdated
}
public enum GroomingStage
{
    NotStartedYet,
    PreGrooming,
    ActiveGromming,
    ReactionOfGrooming,
    PostGrooming,
    DoneGrooming
}
public enum MonitorMode
{
    Idle, Scanning, ItemOnfoNormal, ItemInfoThief
}


