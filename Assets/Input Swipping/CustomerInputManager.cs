﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomerInputManager : MonoBehaviour
{
    public Animator anim;
    public float swipeThreshold;

    Vector3 oldMousePos;
    Vector3 currentMousePos;
    Vector3 progression;

    bool swiped;
    public static CustomerInputManager instance;
    
    #region ALL UNITY FUNCTIONS
    private void Awake()
    {
        instance = this;
    }
    void Update()
    {
        if (Application.isMobilePlatform && Input.touchCount != 1)
            return;

        if (Input.GetMouseButtonDown(0))
        {
            swiped = false;
            oldMousePos = Input.mousePosition;
        }

        if (Input.GetMouseButton(0))
        {
            if (!swiped)
            {
                currentMousePos = Input.mousePosition;

                progression = (currentMousePos - oldMousePos) / Screen.width;
                if (Vector3.Magnitude(progression) > swipeThreshold)
                {
                    if (Mathf.Abs(progression.x) > Mathf.Abs(progression.y))
                    {
                        if (progression.x > 0)
                        {
                            swiped = true;
                            CheckGivenInput(DealingResultType.Accepted);
                        }
                        else
                        {
                            swiped = true;
                            CheckGivenInput(DealingResultType.Rejected);
                        }
                    }
                    //else
                    //{
                    //    if (progression.y > 0)
                    //    {
                    //        swiped = true;
                    //        CheckGivenInput(DealingResultType.PoliceCalled);
                    //    }
                    //}

                }
            }

        }
        else
        {
            progression = Vector3.zero;
            swiped = false;
        }
    }


    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    //public override void OnEnable()
    //{
    //    base.OnEnable();
    //    if (currentLevelData.customerType.Equals(CustomerType.THIEF))
    //    {
    //        anim.SetBool("thiefMode", true);
    //    }
    //}

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    public void CheckGivenInput(DealingResultType dealResult)
    {
        Debug.Log(dealResult.ToString());
        //pawnshopManager.customerInputType = customerInputType;
        //CustomerMode customerMode = pawnshopManager.currentLevelData.customerMode;
        switch (dealResult)
        {
            case DealingResultType.Accepted:
                {
                    GameStatesControl.instance.DealAccept();
                    break;
                }
            case DealingResultType.Rejected:
                {
                    GameStatesControl.instance.DealReject();
                    break;
                }
            case DealingResultType.PoliceCalled:
                {
                    break;
                }
        }
        
        //switch (customerInputType)
        //{
        //    case CustomerInputType.ACCEPTED:
        //        switch (customerMode)
        //        {
        //            case CustomerMode.BUYER:
        //                pawnshopManager.OpenShelf(
        //                    ShelfOpeningScenerio.SearchForATypeOfItemInShelf,
        //                    pawnshopManager.currentLevelData.buyerItemType,
        //                    null
        //                    );
        //                break;
        //            case CustomerMode.SELLER:
        //                // Check do you have money to purchase.
        //                //Debug.Log(pawnshopManager.accountManager.GetCurrentBalance());
        //                //Debug.Log(currentLevelData.customerItem);
        //                //CustomerItem customerItem = currentLevelData.customerItemPrefab.GetComponent<CustomerItem>();
        //                CustomerItem customerItem = pawnshopManager.customerManager.GetCustomerItem();
        //                if (pawnshopManager.accountManager.DeductBalance(customerItem.askingValue))
        //                {
        //                    CustomerItemData customerItemData = new CustomerItemData() {
        //                        itemIndex = customerItem.itemId,
        //                        askingPrice = customerItem.askingValue,
        //                        estimatedPrice = customerItem.estimatedValue,
        //                        itemCondition = customerItem.itemCond
        //                    };
        //
        //                    gameplayData.shelfData.customerItemDatas.Add(customerItemData);
        //                    pawnshopManager.SaveGame();
        //                    pawnshopManager.ChangeGameState(GameState.CUSTOMER_ITEM_DEALING_END);
        //                }
        //                break;
        //        }
        //        break;
        //    case CustomerInputType.REJECTED:
        //        pawnshopManager.ChangeGameState(GameState.CUSTOMER_ITEM_DEALING_END);
        //        break;
        //    case CustomerInputType.CALL_POLICE:
        //        if(currentLevelData.customerType.Equals(CustomerType.ROBBER) || currentLevelData.customerType.Equals(CustomerType.THIEF))
        //            pawnshopManager.ChangeGameState(GameState.CUSTOMER_ITEM_DEALING_END);
        //        break;
        //}
    
    }

    #endregion ALL SELF DECLEAR FUNCTIONS

}
