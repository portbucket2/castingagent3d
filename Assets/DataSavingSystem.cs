﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataSavingSystem : MonoBehaviour
{
    public static DataSavingSystem instance;
    public string itemIdsSaved;
    public string[] split;
    public List<int> savedIds;
    public List<int> itemsIds;
    public List<string> savedDataOfModel;
    public List<int> savedDataOfModelVal;
    public int modelsTotalInPodium;
    // Start is called before the first frame update
    private void Awake()
    {
        instance = this;
        //for (int i = 0; i < 2; i++)
        //{
        //    string s = "savedDataOfModel_" + i;
        //    savedDataOfModel.Add(s);
        //    savedDataOfModelVal.Add( i);
        //    PlayerPrefs.SetString("s", ("" + i));
        //}
    }
    void Start()
    {
        //GetItemIdsFromSavedData();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void GetItemIdsFromSavedData()
    {

        //itemIdsSaved = PlayerPrefs.GetString("itemIdsSaved", "-1");
        
        itemIdsSaved = "0_5_3_2_2";
        //PlayerPrefs.SetString("itemIdsSaved", itemIdsSaved);
        //itemIdsSaved = PlayerPrefs.GetString("itemIdsSaved", "0");
        split = itemIdsSaved.Split(char.Parse("_"));
        
        for (int i = 0; i < split.Length; i++)
        {
            int val;
            int.TryParse(split[i], out val);
            savedIds.Add(val);
            if (val >= 0)
            {
                itemsIds.Add(val);
            }
            
        }
    }
    public void SaveModelData()
    {
        
        modelsTotalInPodium = PodiumSystem.instance.modelAvatars.Count;
        for (int i = 0; i < modelsTotalInPodium; i++)
        {
            string s = "savedDataOfModel_" + i;
            AvatarAttributes avatarAttr = PodiumSystem.instance.modelAvatarScripts[i];
            string longValue = "" + avatarAttr.avatarVisualId + "_" +
                avatarAttr.title + "_" +
                (int)avatarAttr.height + "_" +
                (int)avatarAttr.weight + "_" +
                (int)avatarAttr.skinTone + "_" +
                (int)avatarAttr.makeUpState + "_" +
                (int)avatarAttr.outfitState + "_" +
                avatarAttr.dealValue + "_" +
                avatarAttr.estimatedWorth ;

            PlayerPrefs.SetString(s, ("" + longValue));
            string ss = PlayerPrefs.GetString(s, "x");
            Debug.Log(s + " " + ss);
            split = ss.Split(char.Parse("_"));
        }

        
        PlayerPrefs.SetInt ("modelsTotalInPodium" , modelsTotalInPodium);
    }
    public void SaveLatestModelData(int i)
    {

        modelsTotalInPodium = PodiumSystem.instance.modelAvatars.Count;
        //int i = modelsTotalInPodium - 1;
        string s = "savedDataOfModel_" + i;
        AvatarAttributes avatarAttr = PodiumSystem.instance.modelAvatarScripts[i];
        string longValue = "" + avatarAttr.avatarVisualId + "_" +
            avatarAttr.title + "_" +
            (int)avatarAttr.height + "_" +
            (int)avatarAttr.weight + "_" +
            (int)avatarAttr.skinTone + "_" +
            (int)avatarAttr.makeUpState + "_" +
            (int)avatarAttr.outfitState + "_" +
            avatarAttr.dealValue + "_" +
            avatarAttr.estimatedWorth;

        PlayerPrefs.SetString(s, ("" + longValue));
        string ss = PlayerPrefs.GetString(s, "x");
        Debug.Log(s + " " + ss);
        split = ss.Split(char.Parse("_"));


        PlayerPrefs.SetInt("modelsTotalInPodium", modelsTotalInPodium);
    }

    public void SavedDataRetrieveForPodium()
    {
        modelsTotalInPodium = PlayerPrefs.GetInt("modelsTotalInPodium",0);
        for (int i = 0; i < modelsTotalInPodium; i++)
        {
            string s = "savedDataOfModel_" + i;
            string ss = PlayerPrefs.GetString(s, "x");
            string[] splitVal = ss.Split(char.Parse("_"));
            int val;
            int.TryParse(splitVal[0], out val);

            PodiumSystem.instance.AddAModelToPodium(AvatarManager.instance.avatarsInside[val]);
            AvatarAttributes avatarAttr = PodiumSystem.instance.modelAvatarScripts[i];
            

            avatarAttr.title = splitVal[1];

            int.TryParse(splitVal[2], out val);
            avatarAttr.height = (Height)val;

            int.TryParse(splitVal[3], out val);
            avatarAttr.weight = (Weight)val;

            int.TryParse(splitVal[4], out val);
            avatarAttr.skinTone = (SkinTone)val;

            int.TryParse(splitVal[5], out val);
            avatarAttr.makeUpState = (MakeUpState)val;

            int.TryParse(splitVal[6], out val);
            avatarAttr.outfitState = (OutfitState)val;

            int.TryParse(splitVal[7], out val);
            avatarAttr.dealValue = val;

            int.TryParse(splitVal[8], out val);
            avatarAttr.estimatedWorth = val;

            avatarAttr.HealthTypeSettingAtInitialization();
            avatarAttr.GetComponent<AvatarShadersControl>().MatDuplication();
            avatarAttr.GetComponent<AvatarShadersControl>().ShadersUpdatingAsCurrentCondition();
        }
    }

    public int LevelIndexGet()
    {
        int level = 0;
        level = PlayerPrefs.GetInt("levelIndex", 0);
        return level;
    }
    public void LevelIndexSet(int levelIndex)
    {
        PlayerPrefs.SetInt("levelIndex", levelIndex);
    }
    public int LevelIndexDisplayedGet()
    {
        int levelDis = 0;
        levelDis = PlayerPrefs.GetInt("levelIndexDisplayed", 0);
        return levelDis;
    }
    public void LevelIndexDisplayedSet(int levelIndexDis)
    {
        PlayerPrefs.SetInt("levelIndexDisplayed", levelIndexDis);
    }
    public int BalanceurrentGet()
    {
        int balance = 0;
        balance = PlayerPrefs.GetInt("balanceCurrent", 1000);
        return balance;
    }
    public void BalanceCurrentSet(int balanceCurrent)
    {
        PlayerPrefs.SetInt("balanceCurrent", balanceCurrent);
    }
}
