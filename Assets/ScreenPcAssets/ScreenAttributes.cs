﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ScreenAttributes : MonoBehaviour

{
    public static ScreenAttributes instance;
    public GameObject[] monitorPanels;

    public TextMeshProUGUI ModelNameText;
    public TextMeshProUGUI ModelHeightText;
    public TextMeshProUGUI ModelWeightText;
    public TextMeshProUGUI ModelSkinToneText;
    public TextMeshProUGUI ModelEstimationPriceText;

    public TextMeshProUGUI thiefItemNameText;
    public TextMeshProUGUI thiefItemEstimationPriceText;

    public GameObject[] monitorAllPanels;
    public Image customerPP;

    
    

    void Awake()
    {
        instance = this;
    }
    public void MonitorInfoUpdateNew(CustomerAttributes customerAttr)
    {
        AvatarAttributes avatarAttr =   customerAttr.avatarVisual.GetComponent<AvatarAttributes>();
        ModelNameText           .text =""+ avatarAttr.title  ;
       ModelHeightText         .text =""+  avatarAttr.height ;
       ModelWeightText         .text =""+  avatarAttr.weight    ;
       ModelSkinToneText       .text =""+  avatarAttr.skinTone  ;
       ModelEstimationPriceText.text ="$ "+ avatarAttr.estimatedWorth  ;

        customerPP.sprite = avatarAttr.charPP;

        //itemEstimationPriceText.text = "$" + customerItem.estimatedValue;
        //thiefItemEstimationPriceText.text = "$" + customerItem.estimatedValue;
    }
    void MonitorPanelsGetRight(int index)
    {
        for (int i = 0; i < monitorAllPanels.Length; i++)
        {
            monitorPanels[i].SetActive(i == index);
        }
    }

    public void MonitorDisplay(MonitorMode monitorMode )
    {
        //if(customerItem != null)
        //{
        //    MonitorInfoUpdateNew(customerItem);
        //}
        

        switch (monitorMode)
        {
            case MonitorMode.Idle:
                MonitorPanelsGetRight(0);
                break;
            case MonitorMode.Scanning:
                MonitorPanelsGetRight(1);
                break;
            case MonitorMode.ItemOnfoNormal:
                MonitorPanelsGetRight(2);
                break;
            case MonitorMode.ItemInfoThief:
                MonitorPanelsGetRight(3);
                break;
        }
    }
}
