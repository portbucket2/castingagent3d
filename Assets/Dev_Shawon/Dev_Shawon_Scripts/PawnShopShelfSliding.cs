﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PawnShopShelfSliding : MonoBehaviour
{
    public GameObject ObjToslide;
    public Vector3 touchInPosition;
    public Vector3 targetPosition;
    public ShelfTouchInputs shelfTouchInputs;
    float slideLowerLimitX;
    float slideUpperLimitX;

    public float snapSpeed;

    public float sideVelocity;
    //public int currentSnappedShelfIndex;

    public bool velocityRunMode;
    //public PawnShopShelfManager pawnShopShelfManager;
    public Text swipeSpeedText;
    public int shelfCount;
    public float shelfWidth;
    public bool snappedBuyPos;
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    void Awake()
   {
        //pawnShopShelfManager = GetComponent<PawnShopShelfManager>();
        snapSpeed = 10;
   }
   //
   //// Start is called before the first frame update
   void Start()
   {
        slideLowerLimitX = (shelfWidth * 0.25f);
        
   }
   
    private void Update()
    {
        //ObjToslide.transform.position = Vector3.Lerp(ObjToslide.transform.position, targetPosition, Time.deltaTime* snapSpeed);
        //swipeSpeedText.text = "" + sideVelocity;
        if (shelfTouchInputs.shelfTouchState == ShelfTouchInputs.ShelfTouchState.touchMoving)
        {
            snapSpeed = 20;
            sideVelocity = 0;
            targetPosition = touchInPosition + new Vector3(shelfTouchInputs.progressionX * 7, 0, 0);
            //float limitX = Mathf.Clamp(targetPosition.x, -(((gameplayData.shelfData.shelfCount -1 )* pawnShopShelfManager.shelfWidth)+(pawnShopShelfManager.shelfWidth*0.25f)), slideLowerLimitX);
            //targetPosition = new Vector3(limitX, targetPosition.y, targetPosition.z);
            //snapSpeed = 20f;
            //ObjToslide.transform.localPosition = Vector3.Lerp(ObjToslide.transform.localPosition, targetPosition, Time.deltaTime * snapSpeed);
            
        }
        else if(shelfTouchInputs.shelfTouchState == ShelfTouchInputs.ShelfTouchState.idle)
        {
            snapSpeed = 10;
            sideVelocity = Mathf.MoveTowards(sideVelocity, 0, Time.deltaTime * 5f);
            //if (velocityRunMode)
            //{
            //    //sideVelocity -= Time.deltaTime*5;
            //    sideVelocity = Mathf.MoveTowards(sideVelocity, 0, Time.deltaTime * 2);
            //    targetPosition.x += sideVelocity;
            //    if (Mathf.Abs(sideVelocity) < 0.05f)
            //    {
            //        velocityRunMode = false;
            //        sideVelocity = 0;
            //        GoSnapToShelf(targetPosition);
            //    }
            //    float limitX = Mathf.Clamp(targetPosition.x, -(((PawnShopShelfManager.instance.shelfCount - 1) * PawnShopShelfManager.instance.shelfWidth) + (PawnShopShelfManager.instance.shelfWidth * 0.25f)), slideLowerLimitX);
            //    targetPosition = new Vector3(limitX, targetPosition.y, targetPosition.z);
            //    ObjToslide.transform.position = Vector3.Lerp(ObjToslide.transform.position, targetPosition, Time.deltaTime * snapSpeed);
            //}
            //else
            //{
            //    snapSpeed = 10f;
            //    ObjToslide.transform.position = Vector3.Lerp(ObjToslide.transform.position, targetPosition, Time.deltaTime * snapSpeed);
            //}
            //snapSpeed = 10f;
            targetPosition.x += sideVelocity*1f *Time.deltaTime;
            if(sideVelocity == 0.0f && !snappedBuyPos && velocityRunMode)
            {
                GoSnapToShelf(targetPosition);
                snappedBuyPos=  true;
                velocityRunMode = false;
            }
            //ObjToslide.transform.localPosition = Vector3.Lerp(ObjToslide.transform.localPosition, targetPosition, Time.deltaTime * snapSpeed);

        }
        else if (shelfTouchInputs.shelfTouchState == ShelfTouchInputs.ShelfTouchState.touched)
        {
            snapSpeed = 10;
            sideVelocity = 0;
            //ObjToslide.transform.localPosition = Vector3.Lerp(ObjToslide.transform.localPosition, targetPosition, Time.deltaTime * snapSpeed);
        }
        if(targetPosition.x > slideLowerLimitX || targetPosition.x <(-(((shelfCount - 1) * shelfWidth) + (shelfWidth * 0.25f))))
        {
            sideVelocity = 0;
        }
        slideUpperLimitX = -(((shelfCount - 1) * shelfWidth) + (shelfWidth * 0.25f));
        float limitX = Mathf.Clamp(targetPosition.x, slideUpperLimitX, slideLowerLimitX);
        targetPosition = new Vector3(limitX, targetPosition.y, targetPosition.z);
        ObjToslide.transform.localPosition = Vector3.Lerp(ObjToslide.transform.localPosition, targetPosition, Time.deltaTime * snapSpeed);

        if((Vector3.Distance(ObjToslide.transform.localPosition, targetPosition)< 0.01f))
        {
            snappedBuyPos = false;
        }



    }
    //public void GetTargetPos(bool touched, Vector3 targetPos)
    //{
    //    if (touched)
    //    {
    //        targetPosition = targetPos;
    //    }
    //}
    public void GetObjTouchInPosition()
    {
        touchInPosition =  targetPosition;
    }
    public void GoSnapToShelf(Vector3 currentTargetPos)
    {
        int shelfIndex = (int)((Mathf.Abs(currentTargetPos.x -(shelfWidth/2))) / shelfWidth);
        targetPosition = new Vector3(-shelfWidth*shelfIndex , targetPosition.y , targetPosition.y);
        PodiumSystem.instance. currentSnappedShelfIndex = shelfIndex;
        UiManagePodium.instance.UiPodiumTextValuesUpdate();
        Debug.Log("Snapped by position");
    }
    public void GoSnapToShelf(int shelfIndex)
    {
        
        targetPosition = new Vector3(-shelfWidth * shelfIndex, targetPosition.y, targetPosition.y);
        PodiumSystem.instance.currentSnappedShelfIndex = shelfIndex;
        UiManagePodium.instance.UiPodiumTextValuesUpdate();
        Debug.Log("Snapped by index");
    }
    public void GetSideVelocity()
    {
        velocityRunMode = true;
        sideVelocity = (-ObjToslide.transform.position.x + targetPosition.x)*1;
        sideVelocity = Mathf.Clamp(sideVelocity, -0.5f, 0.5f);
    }
    public void GetSideVelocity(float vel)
    {
        velocityRunMode = true;
        sideVelocity = vel;
        sideVelocity = Mathf.Clamp(sideVelocity, -15f, 15f);
        //Debug.Log("sideVel " + sideVelocity);
    }
    public void KillSideVelocity()
    {
        velocityRunMode = false;
        sideVelocity = 0;
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS


    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS


    #endregion ALL SELF DECLEAR FUNCTIONS

}
