﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShelfTouchInputs : MonoBehaviour
{
    #region ALL UNITY FUNCTIONS
    public Vector2 touchInPos;
    public Vector2 touchPos;
    public Vector2 touchOutPos;

    public GameObject objectToslide;
    public bool touched;
    public bool touchMoving;
    public PawnShopShelfSliding pawnShopShelfSliding;
    public float progressionX;

    public Vector2 mousePointFollowPos;
    public float scrollSpeed;
    public Text touchTestText;
    public enum ShelfTouchState
    {
        idle, touched , touchMoving, stationary
    }
    public ShelfTouchState shelfTouchState;

    public bool touchBolckedForPodium;
    // Awake is called before Start
    void Awake()
    {
        
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Update()
    {
        //touchTestText.text = "" + touched+" " +shelfTouchState;
        if (touchBolckedForPodium)
        {
            return;
        }

#if UNITY_ANDROID || UNITY_IOS
        if (Input.touchCount == 1)
        {
            Touch touch = Input.GetTouch(0);
            MousePointFollowInMobile(touch);
            if (touch.phase == TouchPhase.Began)
            {
                touched = true;
                shelfTouchState = ShelfTouchState.touched;
                touchInPos = touch.position;
                progressionX = 0;
                pawnShopShelfSliding.KillSideVelocity();
                pawnShopShelfSliding.GetObjTouchInPosition();



            }
            if (touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary)
            {
                if (touched)
                {
                    touchPos = touch.position;
                    progressionX = (touchPos.x - touchInPos.x) / (Screen.width);
                    //pawnShopShelfSliding(touched , )
                    if (Mathf.Abs(progressionX) > 0.05f)
                    {
                        touchMoving = true;
                        shelfTouchState = ShelfTouchState.touchMoving;
                    }
                    
                }
            }
            else
            {
                //touched = false;
                touchMoving = false;
                //shelfTouchState = ShelfTouchState.idle;
            }
            if (touch.phase == TouchPhase.Ended)
            {

                touchOutPos = touch.position;
                progressionX = 0;
                //pawnShopShelfSliding.GetSideVelocity();
                if (touched)
                {
                    pawnShopShelfSliding.GetSideVelocity(scrollSpeed);
                }
                shelfTouchState = ShelfTouchState.idle;
                touched = false;
            }

        }


#endif

#if UNITY_EDITOR

        MousePointFollow();
        if (Input.GetMouseButtonDown(0))
        {
            touched = true;
            shelfTouchState = ShelfTouchState.touched;
            touchInPos = Input.mousePosition;
            progressionX = 0;
            pawnShopShelfSliding.KillSideVelocity();
            pawnShopShelfSliding.GetObjTouchInPosition();

        }
        if (Input.GetMouseButton(0))
        {
            //touched = true;
            if (touched)
            {
                touchPos = Input.mousePosition;
                progressionX = (touchPos.x - touchInPos.x) / (Screen.width);
                //pawnShopShelfSliding(touched , )
                if (Mathf.Abs(progressionX) > 0.05f)
                {
                    touchMoving = true;
                    shelfTouchState = ShelfTouchState.touchMoving;
                }
            }
            
        }
        else
        {
            //touched = false;
            touchMoving = false;
            shelfTouchState = ShelfTouchState.idle;
        }
        if (Input.GetMouseButtonUp(0))
        {
            
            touchOutPos = Input.mousePosition;
            progressionX = 0;
            if (touched)
            {
                pawnShopShelfSliding.GetSideVelocity(scrollSpeed);
            }
            //pawnShopShelfSliding.GoSnapToShelf(pawnShopShelfSliding.targetPosition);
            shelfTouchState = ShelfTouchState.idle;
            touched = false;
        }
#endif
    }
    public void TouchBlockInPodium()
    {
        touchBolckedForPodium = true;
        UiManagePodium.instance.swipeLeftRightPanel.SetActive(false);
    }
    public void TouchUnBlockInPodium()
    {
        touchBolckedForPodium = false;
        UiManagePodium.instance.swipeLeftRightPanel.SetActive(true);
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS


    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS
    public void MousePointFollow()
    {
        if (Input.GetMouseButtonDown(0))
        {
            mousePointFollowPos = Input.mousePosition;
            scrollSpeed = 0;
        }
        else if (Input.GetMouseButton(0))
        {
            mousePointFollowPos.x = Mathf.Lerp(mousePointFollowPos.x, Input.mousePosition.x, Time.deltaTime * 5f);
            scrollSpeed = -(mousePointFollowPos.x - Input.mousePosition.x) * 0.02f;
        }


    }
    public void MousePointFollowInMobile(Touch touch)
    {
        if (touch.phase == TouchPhase.Began)
        {
            mousePointFollowPos = touch.position;
            scrollSpeed = 0;
        }
        else if (touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary)
        {
            mousePointFollowPos.x = Mathf.Lerp(mousePointFollowPos.x, touch.position.x, Time.deltaTime * 5f);
            scrollSpeed = -(mousePointFollowPos.x - touch.position.x) * 0.02f;
        }


    }

    private void OnDisable()
    {
        touched = false;
        touchMoving = false;
        progressionX = 0;
        shelfTouchState = ShelfTouchState.idle;
    }
    #endregion ALL SELF DECLEAR FUNCTIONS

}
